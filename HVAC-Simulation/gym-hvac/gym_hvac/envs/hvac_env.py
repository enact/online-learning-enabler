import datetime as dt
import time
import csv
import numpy as np
import pandas as pd
import math
import gym
from gym import spaces
from gym_hvac.envs.occupancy_utils import OccupancyPatternGenerator


class HvacEnv(gym.Env):
    """
    HvacEnv is an OpenAI Gym environment for simulating the thermal dynamics of a building/room.
    """

    def __init__(self,
                 total_steps=524_100,
                 reward_function="standard",
                 reward_weighting=0.5,
                 gradient_beta=0.9,
                 fps_printing_interval=30,
                 use_thermostat=False,
                 use_weather_data=True,
                 occupancy_pattern=1,
                 use_probabilistic_occupancy=True,
                 occupancy_prediction_horizon=30,
                 outdoor_temperature_bias=15,
                 outdoor_temperature_amplitude=10,
                 set_point_boundaries=1,
                 initial_set_point=20,
                 initial_indoor_temperature=20,
                 house_length=6.140,
                 house_width=6.891,
                 house_height=2.388,
                 percentage_window_area=0.95,
                 u_value_wall=1.7,
                 u_value_window=0.19,
                 cp_air=1005.4,
                 density_air=1.2250,
                 heating_temperature=25,
                 cooling_temperature=12,
                 min_temperature=-10,
                 max_temperature=50,
                 experiment_name=None,
                 observations_space_combination=None,
                 seed=None,
                 save_log=False,
                 log_filename="csvfile.csv",
                 simplified_dynamics=False,
                 continuous_action_space=False,
                 observation_space_normalization=False,
                 reward_scaling=1
                 ):

        if observations_space_combination is None:
            observations_space_combination = [1, 1, 1, 1, 0, 0, 0, 1, 1]

        np.random.seed(seed)
        self.MIN_TEMPERATURE = min_temperature
        self.MAX_TEMPERATURE = max_temperature

        """
        :param total_steps: actual time step
        :param gradient_beta: hyper parameter for the gradient calculation (exponential moving average)
        :param tensorboard_logging: activate/deactivate tensorboard logging
        :param fps_printing_interval: define how often experiment infos should be shown
        """
        assert 0 <= gradient_beta <= 1
        self.gradient_beta = gradient_beta  # hyper parameter for the gradient calculation (exponential moving average)
        assert 0 <= reward_weighting <= 1
        self.reward_weighting = reward_weighting

        # logging
        self.save_log = save_log
        self.log_filename = log_filename

        self.simplified_dynamics = simplified_dynamics

        self.experiment_name = experiment_name
        self.observations_space_combination = observations_space_combination
        self.observation_space_normalization = observation_space_normalization

        # fps logging
        self.fps_printing_interval = fps_printing_interval
        self.fps_start_time = None
        self.fps_counter = None
        self.average_fps = None

        self.continuous_action_space = continuous_action_space

        if self.continuous_action_space:
            self.action_space = spaces.Box(np.array([-1.]), np.array([1.]))
        else:
            self.action_space = spaces.Discrete(7)

        # gym default variables
        self.observation = None
        self.done = None
        self.info = None

        # action
        self.last_action = None
        self.last_heating_action = None
        self.last_cooling_action = None
        self.last_air_flow_factor = None
        self.last_air_flow_rate = None

        # env variables #################################
        self.time_step = None
        self.total_steps = total_steps
        self.reward_function = reward_function
        self.reward_scaling = reward_scaling

        # temperatures
        self.indoor_temperature = None  # 1st observation value
        self.outdoor_temperature = None  # 2nd observation value
        self.set_point = None  # 3rd observation value
        self.deviation_from_set_point = None  # 4th observation value

        # history
        self.last_indoor_temperature = None
        self.last_outdoor_temperature = None
        self.last_deviation_from_set_point = None

        # temperature gradients
        self.indoor_temperature_gradient = None  # 1 th observation value
        self.outdoor_temperature_gradient = None  # 1 th observation value
        self.deviation_from_set_point_gradient = None  # 1 th observation value

        # occupancy
        self.occupancy = None  # 8 th observation value
        self.occupancy_prediction = None  # 9 th observation value
        self.occupancy_phase = None

        # reward
        self.reward = None
        self.energy_reward = None
        self.thermal_comfort_reward = None
        self.cumulative_energy_reward = None
        self.cumulative_thermal_comfort_reward = None
        self.cumulative_reward = None
        ###################################################

        if self.observation_space_normalization == False:

            low = [self.MIN_TEMPERATURE] * 7 + [0, 0]
            high = [self.MAX_TEMPERATURE] * 7 + [1, 1]

            # deviation_from_setpoint needs special treatment
            low[3] = -(abs(self.MIN_TEMPERATURE) + abs(self.MAX_TEMPERATURE))
            high[3] = abs(self.MIN_TEMPERATURE) + abs(self.MAX_TEMPERATURE)

        else:
            low = [-1] * 7 + [0, 0]
            high = [1] * 7 + [1, 1]

            # deviation_from_setpoint needs special treatment
            low[3] = -1
            high[3] = 1

        if self.observations_space_combination is None:
            temp_low = low
            temp_high = high
        else:
            temp_low = []
            temp_high = []
            for index, value in enumerate(self.observations_space_combination):
                if value == 1:
                    temp_low.append(low[index])
                    temp_high.append(high[index])

        self.observation_space = spaces.Box(low=np.float32(temp_low),
                                            high=np.float32(temp_high))

        # convergence metrics
        self.indoor_temperature_as_desired = None

        self.USE_THERMOSTAT = use_thermostat
        self.SET_POINT_BOUNDARIES = set_point_boundaries
        self.OCCUPANCY_PREDICTION_HORIZON = occupancy_prediction_horizon
        self.USE_REAL_WEATHER_DATA = use_weather_data
        self.OUTDOOR_TEMPERATURE_BIAS = outdoor_temperature_bias
        self.OUTDOOR_TEMPERATURE_AMPLITUDE = outdoor_temperature_amplitude

        if self.USE_THERMOSTAT:
            self.thermostat_heating = None
        self.thermostat_cooling = None

        if self.USE_REAL_WEATHER_DATA:
            self.real_weather_outdoor_temperatures = pd.read_csv("./gym-hvac/gym_hvac/envs/kubik_yearly_data_2017.csv")

        occupancy_pattern_generator = OccupancyPatternGenerator(occupancy_pattern=occupancy_pattern,
                                                                horizon=occupancy_prediction_horizon,
                                                                use_probabilistic_occupancy=use_probabilistic_occupancy)
        self.generated_data_of_occupancy_pattern, self.generated_data_of_occupancy_predictions = occupancy_pattern_generator.generate_occupancy_patterns(
            total_steps=total_steps)

        # ## THERMAL CONFIGURATION OF THE HOUSE ## #
        self.INITIAL_SET_POINT = initial_set_point
        self.INITIAL_INDOOR_TEMPERATURE = initial_indoor_temperature
        # -------------------------------
        # Define the house geometry
        self.HOUSE_LENGTH = house_length
        # House width = 10 m
        self.HOUSE_WIDTH = house_width
        # House height = 4 m
        self.HOUSE_HEIGHT = house_height
        self.WINDOW_AREA = self.HOUSE_LENGTH * self.HOUSE_HEIGHT + self.HOUSE_WIDTH * self.HOUSE_HEIGHT * percentage_window_area
        self.WALL_AREA = self.HOUSE_LENGTH * self.HOUSE_HEIGHT + self.HOUSE_WIDTH * self.HOUSE_HEIGHT * (
                1 - percentage_window_area)
        # -------------------------------
        # Define the type of insulation used
        self.U_VALUE_WALL = u_value_wall  # W/(m^2 * K)
        self.U_VALUE_WINDOW = u_value_window  # W/(m^2 * K)
        self.R_VALUE_WINDOW = 1 / (self.U_VALUE_WINDOW * self.WINDOW_AREA)
        self.R_VALUE_WALL = 1 / (self.U_VALUE_WALL * self.WALL_AREA)
        # -------------------------------
        # Determine the equivalent thermal resistance for the whole building
        self.R_HOUSE = self.R_VALUE_WALL * self.R_VALUE_WINDOW / (self.R_VALUE_WALL + self.R_VALUE_WINDOW)
        # -------------------------------
        # C = cp of air (273 K) = 1005.4 J/kg-K
        self.CP_AIR = cp_air
        # -------------------------------
        # Density of air at sea level = 1.2250 kg/m^3
        self.DENSITY_AIR = density_air
        # -------------------------------
        # Determine total internal air mass = M
        self.M = (self.HOUSE_LENGTH * self.HOUSE_WIDTH * self.HOUSE_HEIGHT) * self.DENSITY_AIR
        # -------------------------------
        # the temperature of air, exiting the fan coils
        self.HEATING_TEMPERATURE = heating_temperature
        self.COOLING_TEMPERATURE = cooling_temperature
        # -------------------------------

    def _thermostat_action(self):

        if not self.occupancy:
            self.thermostat_heating = False
            self.thermostat_cooling = False
            return 0

        if self.deviation_from_set_point < -self.SET_POINT_BOUNDARIES:
            self.thermostat_heating = True
            self.thermostat_cooling = False
            return 1
        elif self.deviation_from_set_point > self.SET_POINT_BOUNDARIES:
            self.thermostat_heating = False
            self.thermostat_cooling = True
            return 2
        else:
            if self.thermostat_heating:
                return 1
            if self.thermostat_cooling:
                return 2
            if not self.thermostat_heating:
                return 0

    def _set_action(self, action):
        self.last_action = action
        if self.USE_THERMOSTAT:
            thermostat_action = self._thermostat_action()

            if thermostat_action == 0:
                self.last_heating_action = 0
                self.last_heating_action_sd = 0
                self.last_cooling_action = 0
                self.last_cooling_action_sd = 0
                self.last_air_flow_rate = 0
            elif thermostat_action == 1:
                self.last_heating_action = 1
                self.last_heating_action_sd = 1
                self.last_cooling_action = 0
                self.last_cooling_action_sd = 0
                self.last_air_flow_rate = (450 / 60)
            elif thermostat_action == 2:
                self.last_heating_action = 0
                self.last_heating_action_sd = 0
                self.last_cooling_action = 1
                self.last_cooling_action_sd = 1
                self.last_air_flow_rate = (450 / 60)
        else:
            if self.continuous_action_space:
                # fully continuous case
                if np.isclose(action, 0, atol=0.1):
                    self.last_cooling_action = 0
                    self.last_cooling_action_sd = 0
                    self.last_heating_action = 0
                    self.last_heating_action_sd = 0
                    self.last_air_flow_rate = 0
                elif action < 0:
                    self.last_cooling_action = 1
                    self.last_cooling_action_sd = np.abs(action)
                    self.last_heating_action = 0
                    self.last_heating_action_sd = 0
                    self.last_air_flow_rate = (450 / 60) * np.abs(action)
                elif action > 0:
                    self.last_cooling_action = 0
                    self.last_cooling_action_sd = 0
                    self.last_heating_action = 1
                    self.last_heating_action_sd = np.abs(action)
                    self.last_air_flow_rate = (450 / 60) * np.abs(action)
            else:

                if action == 3:
                    self.last_cooling_action = 0
                    self.last_cooling_action_sd = 0
                    self.last_heating_action = 0
                    self.last_heating_action_sd = 0
                    self.last_air_flow_rate = (0 / 60)
                elif action == 2:
                    self.last_cooling_action = 1
                    self.last_cooling_action_sd = 1 / 3
                    self.last_heating_action = 0
                    self.last_heating_action_sd = 0
                    self.last_air_flow_rate = (260 / 60)
                elif action == 1:
                    self.last_cooling_action = 1
                    self.last_cooling_action_sd = 2 / 3
                    self.last_heating_action = 0
                    self.last_heating_action_sd = 0
                    self.last_air_flow_rate = (350 / 60)
                elif action == 0:
                    self.last_cooling_action = 1
                    self.last_cooling_action_sd = 1
                    self.last_heating_action = 0
                    self.last_heating_action_sd = 0
                    self.last_air_flow_rate = (450 / 60)
                elif action == 4:
                    self.last_cooling_action = 0
                    self.last_cooling_action_sd = 0
                    self.last_heating_action = 1
                    self.last_heating_action_sd = 1 / 3
                    self.last_air_flow_rate = (260 / 60)
                elif action == 5:
                    self.last_air_flow_factor = (2 / 3)
                    self.last_cooling_action = 0
                    self.last_cooling_action_sd = 0
                    self.last_heating_action = 1
                    self.last_heating_action_sd = 2 / 3
                    self.last_air_flow_rate = (350 / 60)
                elif action == 6:
                    self.last_cooling_action = 0
                    self.last_cooling_action_sd = 0
                    self.last_heating_action = 1
                    self.last_heating_action_sd = 1
                    self.last_air_flow_rate = (450 / 60)

        # set correct heating and cooling temperatures considering the current air flow
        if self.last_air_flow_rate >= (450 / 60):
            self.HEATING_TEMPERATURE = 40.
            self.COOLING_TEMPERATURE = 16.
        elif self.last_air_flow_rate >= (350 / 60):
            self.HEATING_TEMPERATURE = 39.
            self.COOLING_TEMPERATURE = 16.5
        else:
            self.HEATING_TEMPERATURE = 38.
            self.COOLING_TEMPERATURE = 17.

    def step(self, action):
        """
        In this function the variables for the next time step are computed based on the given action.
        """

        self._set_action(action)

        # remember the last values
        self.last_indoor_temperature = self.indoor_temperature
        self.last_outdoor_temperature = self.outdoor_temperature
        self.last_deviation_from_set_point = self.deviation_from_set_point

        # update the indoor temperature and outdoor temperature
        if self.simplified_dynamics:
            self.update_temperatures_simplified()
        else:
            self.update_temperatures()

        self.time_step += 1

        # detect change in occupancy pt.1
        last_occupancy = self.occupancy

        # calculating the values of the next observation
        self.deviation_from_set_point = self.indoor_temperature - self.set_point
        self.occupancy = self.generated_data_of_occupancy_pattern[self.time_step]
        self.occupancy_prediction = self.generated_data_of_occupancy_predictions[self.time_step]

        # detect change in occupancy pt.2
        if last_occupancy != self.occupancy:
            self.occupancy_phase += 1

        # setting the gradients
        self.indoor_temperature_gradient = self.gradient_beta * self.indoor_temperature_gradient + (
                1 - self.gradient_beta) * (
                                                   self.indoor_temperature - self.last_indoor_temperature)
        self.outdoor_temperature_gradient = self.gradient_beta * self.indoor_temperature_gradient + (
                1 - self.gradient_beta) * (
                                                    self.outdoor_temperature - self.last_outdoor_temperature)
        self.deviation_from_set_point_gradient = self.gradient_beta * self.indoor_temperature_gradient + (
                1 - self.gradient_beta) * (
                                                         self.deviation_from_set_point - self.last_deviation_from_set_point)

        # new observation
        self.observation = self._get_observation()
        self.reward = self._reward()

        # check if the action is correct to the researchers expectation
        self._convergence_metrics()

        if self.save_log:
            self._log()

        self.print_fps()

        if self.time_step > self.total_steps:
            self.reward = 0.
            self.done = True

        return self.observation, self.reward * self.reward_scaling, self.done, self.info

    def _convergence_metrics(self):
        """
        Additional debug metrics.
        """
        # indoor_temperature_as_desired metric
        if np.abs(self.indoor_temperature - self.set_point) <= self.SET_POINT_BOUNDARIES or not self.occupancy:
            self.indoor_temperature_as_desired = True
        else:
            self.indoor_temperature_as_desired = False

    def _reward(self):

        energy_penalty = -10.
        heating_penalty = 1 * energy_penalty
        cooling_penalty = 1 * energy_penalty

        sp_diff = np.abs(self.indoor_temperature - self.set_point)

        if self.occupancy:
            if sp_diff < self.SET_POINT_BOUNDARIES:
                self.thermal_comfort_reward = 100
                self.energy_reward = self.last_cooling_action_sd * cooling_penalty + self.last_heating_action_sd * heating_penalty
            else:
                self.thermal_comfort_reward = -math.pow(sp_diff, 2)
                self.energy_reward = 0
        else:
            self.thermal_comfort_reward = 0
            self.energy_reward = 10 * self.last_cooling_action_sd * cooling_penalty + 10 * self.last_heating_action_sd * heating_penalty

        reward = self.thermal_comfort_reward + self.energy_reward

        self.cumulative_energy_reward += self.energy_reward
        self.cumulative_thermal_comfort_reward += self.thermal_comfort_reward
        self.cumulative_reward += reward

        return reward

    def print_fps(self):
        """
        Function to print information about the simulation.
        """

        if self.fps_start_time is None:
            self.fps_start_time = time.time()
            self.fps_counter = 0

        self.fps_counter += 1

        if (time.time() - self.fps_start_time) > self.fps_printing_interval:
            fps = round(self.fps_counter / ((time.time() - self.fps_start_time) + 0.0000001), 1)

            remaining_steps = (self.total_steps - self.time_step)
            remaining_seconds = remaining_steps / fps
            remaining_time = time.strftime('%H:%M:%S', time.gmtime(remaining_seconds))

            now = dt.datetime.now()
            delta = dt.timedelta(seconds=remaining_seconds)
            t = now.time()
            end_time = (dt.datetime.combine(dt.date(1, 1, 1), t) + delta).time().strftime("%H:%M:%S")

            print(
                "time:{}    time steps: {:,d}/{:,d}   FPS: {}    remaining time: {}    end time: {}".format(
                    time.strftime('%H:%M:%S'),
                    self.time_step,
                    self.total_steps,
                    fps,
                    remaining_time,
                    end_time))
            self.fps_counter = 0
            self.fps_start_time = time.time()

    def update_temperatures(self):
        """
        Function for updating indoor and outdoor temperature variables.
        """

        self.indoor_temperature += self.indoor_temperature_delta()
        self.outdoor_temperature = self._outdoor_temperature()

    def update_temperatures_simplified(self):
        """
        Simplified model dynamics.
        """

        # Heating loss/gain durch Unterschied Innen und Außentemp vereinfacht
        if self.indoor_temperature < self.outdoor_temperature and self.indoor_temperature < 50.0:
            self.indoor_temperature += 0.1
        elif self.indoor_temperature > self.outdoor_temperature and self.indoor_temperature > -10.0:
            self.indoor_temperature -= 0.1

        # Heatingflow durch Heizung vereinfacht
        # Wichtig: Angeben bei Experimentaufbau, dass Constraint bzgl. max indoor temp.!
        if 50.0 > self.indoor_temperature > -10.0:
            self.indoor_temperature += 0.5 * self.last_heating_action_sd
            self.indoor_temperature -= 0.5 * self.last_cooling_action_sd

        # Außentemp folgt Standartfunktion (Test mit konstanter Außentemp, Sinuskurve und echtem Tempverlauf)
        self.outdoor_temperature = self._outdoor_temperature()

    def indoor_temperature_delta(self):
        """
        Computation of indoor temperature.
        """
        return (1 / (self.M * self.CP_AIR)) * (self._heating_flow()
                                               + self._cooling_flow()
                                               - self._heating_loss()
                                               )

    def _heating_loss(self):
        """
        Computation of heating loss.
        """
        return (self.indoor_temperature - self.outdoor_temperature) / self.R_HOUSE

    def _heating_flow(self):
        """
        Computation of heat gain through heating.
        """
        if self.HEATING_TEMPERATURE < self.indoor_temperature:
            return 0
        else:
            return (self.HEATING_TEMPERATURE - self.indoor_temperature) \
                   * self.last_air_flow_rate * self.CP_AIR \
                   * self.last_heating_action

    def _cooling_flow(self):
        """
        Computation of heat loss through cooling.
        """
        if self.COOLING_TEMPERATURE > self.indoor_temperature:
            return 0
        else:
            return (self.COOLING_TEMPERATURE - self.indoor_temperature) \
                   * self.last_air_flow_rate * self.CP_AIR \
                   * self.last_cooling_action

    def _outdoor_temperature(self):
        if self.USE_REAL_WEATHER_DATA:
            index = int(self.time_step / 60) % (len(self.real_weather_outdoor_temperatures["Te"]) - 1)
            y0 = self.real_weather_outdoor_temperatures["Te"][index]
            y1 = self.real_weather_outdoor_temperatures["Te"][index + 1]
            x0 = index * 60
            x1 = (index + 1) * 60
            x = np.array([x0, x1])
            y = np.array([y0, y1])
            return np.around(np.interp(self.time_step, x, y), decimals=1)
        else:
            return self.OUTDOOR_TEMPERATURE_AMPLITUDE * math.sin(
                (2 * math.pi / 1440) * self.time_step + 4.5) + self.OUTDOOR_TEMPERATURE_BIAS

    def _get_observation(self):
        """
        Function to output next observation as numpy array.
        """

        indoor_temperature = self.indoor_temperature
        outdoor_temperature = self.outdoor_temperature
        deviation_from_set_point = self.deviation_from_set_point

        if self.observation_space_normalization == True:
            temperature_range = self.MAX_TEMPERATURE - self.MIN_TEMPERATURE
            deviation_range = temperature_range * 2
            indoor_temperature = indoor_temperature / temperature_range
            outdoor_temperature = outdoor_temperature / temperature_range
            deviation_from_set_point = deviation_from_set_point / deviation_range

        observation = [
            indoor_temperature,
            outdoor_temperature,
            self.set_point,
            deviation_from_set_point,
            self.indoor_temperature_gradient,
            self.outdoor_temperature_gradient,
            self.deviation_from_set_point_gradient,
            self.occupancy,
            self.occupancy_prediction,
        ]

        if self.observations_space_combination is None:
            temp_observation = observation
        else:
            temp_observation = []
            for index, value in enumerate(self.observations_space_combination):
                if value == 1:
                    temp_observation.append(observation[index])

        return np.array(temp_observation)

    def reset(self):
        """
        Function to reset the environments. Called once after initialization of the environment.
        """
        self.time_step = 0
        self.indoor_temperature = self.INITIAL_INDOOR_TEMPERATURE
        self.outdoor_temperature = self._outdoor_temperature()
        self.set_point = self.INITIAL_SET_POINT

        self.deviation_from_set_point = self.indoor_temperature - self.set_point
        self.occupancy = self.generated_data_of_occupancy_pattern[self.time_step]
        self.occupancy_prediction = self.generated_data_of_occupancy_predictions[self.time_step]
        self.occupancy_phase = 1

        self.last_indoor_temperature = self.indoor_temperature
        self.last_outdoor_temperature = self.outdoor_temperature
        self.last_deviation_from_set_point = self.deviation_from_set_point

        self.indoor_temperature_gradient = 0.
        self.outdoor_temperature_gradient = 0.
        self.deviation_from_set_point_gradient = 0.
        self.indoor_temperature_gradient = self.gradient_beta * self.indoor_temperature_gradient + (
                1 - self.gradient_beta) * (
                                                   self.indoor_temperature - self.last_indoor_temperature)
        self.outdoor_temperature_gradient = self.gradient_beta * self.indoor_temperature_gradient + (
                1 - self.gradient_beta) * (
                                                    self.outdoor_temperature - self.last_outdoor_temperature)
        self.deviation_from_set_point_gradient = self.gradient_beta * self.indoor_temperature_gradient + (
                1 - self.gradient_beta) * (
                                                         self.deviation_from_set_point - self.last_deviation_from_set_point)

        # gym default variables
        self.observation = self._get_observation()
        self.done = False
        self.info = {}

        self.reward = 0.
        self.energy_reward = 0.
        self.thermal_comfort_reward = 0.
        self.cumulative_energy_reward = 0.
        self.cumulative_thermal_comfort_reward = 0.
        self.cumulative_reward = 0.

        self.indoor_temperature_as_desired = False

        return self.observation

    def render(self, mode='human'):
        """
        Method stub.
        """
        # Render the environment to the screen
        pass

    def _log(self):
        log_data = [
            self.time_step,
            self.indoor_temperature,
            self.outdoor_temperature,
            self.set_point,
            self.deviation_from_set_point,
            self.indoor_temperature_gradient,
            self.outdoor_temperature_gradient,
            self.deviation_from_set_point_gradient,
            self.occupancy,
            self.occupancy_prediction,
            self.occupancy_phase,
            self.last_action,
            self.last_heating_action,
            self.last_cooling_action,
            self.last_air_flow_factor,
            self.last_air_flow_rate,
            self.reward,
            self.energy_reward,
            self.thermal_comfort_reward,
            self.last_heating_action_sd,
            self.last_cooling_action_sd
        ]

        with open(self.log_filename, 'a', newline='') as csv_file:
            writer = csv.writer(csv_file, delimiter=',')
            writer.writerow(log_data)
