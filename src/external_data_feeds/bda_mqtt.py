import random
import time
import json
import sys

import paho.mqtt.client as mqtt
import requests
from threading import Semaphore
import threading
import _thread


def exit_after(s):
    def cdquit(fn_name):
        print('{0} took too long to complete.'.format(fn_name), file=sys.stderr)
        sys.stderr.flush()
        _thread.interrupt_main()

    '''
    use as decorator to exit process if
    function takes longer than s seconds
    '''
    def outer(fn):
        def inner(*args, **kwargs):
            timer = threading.Timer(s, cdquit, args=[fn.__name__])
            timer.start()
            try:
                result = fn(*args, **kwargs)
            finally:
                timer.cancel()
            return result
        return inner
    return outer


class BDAValueSynchronizer:
    def __init__(self):
        self.DEBUG = False

        self.SYNC_EVERY_N_SECONDS = 2
        self.SKIP_N_OBSERVATIONS = 100

        self.BACKEND_BASE_URL = 'http://localhost:8000/api/'
        self.BACKEND_GET_ACTION_ENDPOINT = 'get_environmental_log_data/'

        self.MQTT_BROKER = 'localhost'
        self.MQTT_PORT = 1883

        self.OBSERVATION_LABELS = None

        self.observations = []
        self.number_of_packets_sent_to_mqtt = 0
        self.number_of_packets_sent_to_backend = 0
        self.last_sync_last_timestep = 0
        self.last_occupancy_prediction = 0
        self.semaphore = Semaphore()

        self.mqtt_client = mqtt.Client()
        self.mqtt_client.connect(self.MQTT_BROKER, self.MQTT_PORT)
        self.mqtt_client.on_message = self.on_message
        self.mqtt_client.loop_start()
        self.mqtt_client.subscribe("hvac/observations/drift")

        self.pull_data_from_backend()
        last_sync_time = time.time()

        while True:
            #self.semaphore.acquire()
            if self.DEBUG:
                print('MQTT acquire!')

            if len(self.observations) > self.number_of_packets_sent_to_mqtt:
                current_observation = self.observations[self.number_of_packets_sent_to_mqtt]
                try:
                    self.publish_observation(current_observation)
                except KeyboardInterrupt:
                    print('Could not send observation: ' + str(current_observation))

            if self.DEBUG:
                print('MQTT release!')

            #self.semaphore.release()

            if time.time() - last_sync_time > self.SYNC_EVERY_N_SECONDS:
                if self.DEBUG:
                    print('Sync!')
                self.pull_data_from_backend()
                last_sync_time = time.time()
            else:
                time.sleep(0.004)

    @exit_after(2)
    def publish_observation(self, observation):
        for i in range(len(observation)):
            topic = "hvac/observations/" + self._convert_to_lower_underscore_case(self.OBSERVATION_LABELS[i])
            value = observation[i]

            if self.DEBUG:
                print('Publishing ' + topic)

            if topic == 'hvac/observations/occupancy_prediction' and 'occupancy' not in self.OBSERVATION_LABELS:
                self.mqtt_client.publish(
                    'hvac/observations/occupancy',
                    self._convert_occupancy_prediction_to_occupancy(self.last_occupancy_prediction, value),
                    qos=2
                )
                self.last_occupancy_prediction = value

            self.mqtt_client.publish(topic, value, qos=2)

            if self.DEBUG:
                print('Published ' + topic)

        self.number_of_packets_sent_to_mqtt += 1

    @staticmethod
    def _convert_occupancy_prediction_to_occupancy(last_occ_pred, current_occ_pred):
        if current_occ_pred in [0, 1]:
            return current_occ_pred

        return 0 if last_occ_pred < current_occ_pred else 1

    @staticmethod
    def _convert_to_lower_underscore_case(label):
        return label.replace(' ', '_').lower()

    def submit_external_data(self, title, labels, values):
        url = self.BACKEND_BASE_URL + 'external_data/'
        requests.post(url, json={
            'unique_title': title,
            'labels': labels,
            'data': [[value] for value in values]
        })

    def pull_data_from_backend(self):
        request = requests.get(self.BACKEND_BASE_URL + self.BACKEND_GET_ACTION_ENDPOINT, params={'first_timestep': self.last_sync_last_timestep})
        if request.status_code == 200:
            data_feed = json.loads(request.content)
            self.OBSERVATION_LABELS = data_feed['observation_labels']

            if len(data_feed['observations']) == 0:
                return

            self.last_sync_last_timestep = len(data_feed['observations']) + data_feed['starting_from_timestep']
            self.observations.extend(data_feed['observations'])

    def on_message(self, client, userdata, msg):
        #self.semaphore.acquire()
        if self.DEBUG:
            print('BDA acquire!')
        new_bda_value = float(msg.payload)
        current_timestep = self.number_of_packets_sent_to_mqtt

        if self.number_of_packets_sent_to_backend < current_timestep:
            print('New value: %.2f. Sending from %i until %i.' % (new_bda_value, self.number_of_packets_sent_to_backend, current_timestep))
            self.submit_external_data('Behavioral Drift', ['Behavioral Drift Value'], [new_bda_value for _ in range(self.number_of_packets_sent_to_backend, current_timestep)])
            self.number_of_packets_sent_to_backend = current_timestep
        elif self.DEBUG:
            print('BD-value not new!')

        if self.DEBUG:
            print('BDA release!')

        #self.semaphore.release()


if __name__ == '__main__':  # Only for testing and validation purposes!
    syncer = BDAValueSynchronizer()
