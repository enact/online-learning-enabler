# Online Learning Enabler

The Online Learning Enabler (aka Context-Aware Self-Adaptation Enabler) offers you means to apply online learning techniques (i.e. Reinforcement Learning approaches) to your self-adaptive system for experimental purposes. The corresponding Online Learning Tool comes up with a proper documentation guiding you through the configuration process of the tool, a configuration pane offering options to setup the tool according to your needs (i.e. setting up an arbitrary sequential decision making problem) and a monitoring pane to give you the opportunity to supervise the learning process of the applied online learning technique.

## Installation

* Install [Docker](https://www.docker.com/get-started)
* Run `docker run -d -p 8000:8000 -p 1883:1883 --name OLE enactproject/online_learning_enabler`

## Access Online Learning tool

* Access the dashboard of Online Learning tool via http://localhost:8000
* All variables can also be accessed via MQTT (mqtt://localhost:1883)

# Reinforcement Learning

<center><img src="/static/rl.png" alt="logo enact" width="40%"/></center>
<center>Agent-Environment Interaction Loop. Adapted from "Reinforcement<br>Learning An Introduction," by R. S. Sutton, and A. G. Barto, 2018, p. 48.</center><br>

The Online Learning Enabler solves sequential decision problems using Reinforcement Learning algorithms. Reinforcement Learning is a learning paradigm which decomposes the learning problem into an agent that can interact with an (initially unknown) environment. By perceiving the current state of the environment, the agent decides for actions to take. Depending on the chosen actions the environment transitions to a new state and returns a scalar reward signal, which is used by the agent to evaluate the previous action. Underlying sequential decision making problems do thereby consist of three components which are of importance 
for the algorithm: states, actions & rewards. The entire interaction process is also shown in the figure above.

In your case, your platform/simulation together with the Online Learning Enabler serves as the agent.  The actions are parameter adjustments that influence the behavior of your platform according to certain quality requirements and states are current environmental or contextual variables . Metrics which measure compliance with the given quality requirements are used to calculate the scalar reward signal.

# Tool Description
The tool consists of three different panes: documentation, configuration & monitoring. In addition, the tool offers an API for automatic configuration and easy integration with other tools. 

## API Endpoints

| Endpoint                         | Request Method | Format | Rationale                                                                                                                                                                                                                                                                                                            |
|----------------------------------|----------------|--------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| /api/submit_configuration/       | POST           | JSON   | This endpoint is used to modify the configuration of the algorithm. This includes (1) the type of the algorithm, (2) observation-space and action-space, and (3) the hyperparameter of the chosen algorithm. The new configuration has to be POSTed as JSON.                                                         |
| /api/get_action/                 | POST           | JSON   | This endpoint is used to transmit the current state variables and the last reward to the algorithm. The response of the request contains the action to be executed. Observation and reward have to be POSTed as JSON. The response is a JSON that contains the action.                                               |
| /api/get_environmental_log_data/ | GET            | JSON   | This endpoint is used to obtain logs of the learning process. The query string can contain the parameter first_timestep, which causes only logs after this timestep to be returned. A GET request has to be performed.                                                                                               |
| /api/external_data/              | POST/GET       | JSON   | This endpoint can be used in two ways. Firstly, external data, for example from a connected tool, can be obtained by a GET request. For this purpose, as described above, the query string can contain the parameter first_timestep. Secondly, external data can be transmitted in JSON format using a POST request. |
| /api/annotations/                | GET            | JSON   | This endpoint can be used in two ways. Firstly, all diagram annotations can be obtained by a GET request. Secondly, new annotations can be transmitted in JSON format using a POST request.                                                                                                                          |
| /api/upload_model/               | POST           | JSON   | This endpoint is used to load an existing model and its configuration into the tool. To do so the ZIP file generated by the save() method of Stable-Baselines 3 must be POSTed as a Base64 encoded string inside of a JSON.                                                                                          |

## Documentation

In the documentation section you will find detailed information about the features of the Online Learning Enabler and an example use case to understand how to configure the online learning tool and when it might be helpful to apply online learning techniques based on Reinforcement Learning.

## Configuration

The configuration pane enables you to select an state-of-the-art Reinforcement Learning algorithm, define its hyperparameters, define your learning problem as a Reinforcement Learning problem and setup an interface for communication between your self-adaptive system and the Reinforcement Learning algorithm managed by the Online Learning tool.

Currently there are two algorithms to choose from: Proximal Policy Optimization and Deep Q-Network. Depending on the underlying environment, state and action space have to be defined. Only if the action space is discrete Deep Q-Networks can be used.



## Monitoring

The monitoring pane gives you the opportunity to supervise the learning process. Hereby it is possible to monitor the evolution of state and actions variables from the Reinforcement Learning perspective and system and context variables from the domain perspective of the self-adaptive system. By adjusting the configuration of the RL algorithm and/or revising the underlying learning problem new insights concerning an optimal control policy or at least how existing control policies might be improved can be identified.

# Exemplary Use Case

Consider a thermostat control system that is supposed to maintain a certain building temperature as energy efficient as possible. In addition, the building to be heated is equipped with various sensors that measure the following values:

| Variable Name           | Lower Bound | Upper Bound | Description                                                                   |
|-------------------------|-------------|-------------|-------------------------------------------------------------------------------|
| Indoor Temperature      | -20         | 50          | The current temperature inside the building.                                  |
| Outdoor Temperature     | -20         | 50          | The current temperature outside the building.                                 |
| Setpoint                | -20         | 50          | The temperature preferred by the user.                                        |
| Deviation from Setpoint | 0           | 70          | The difference between the current temperature and the preferred temperature. |
| Occupancy               | 0           | 1           | Whether the user is occupant (1) or not (0).                                  |

The adaptions made by the system consist of activating a heating/air conditioning device with a certain intensity:

| Action Name     | Lower Bound | Upper Bound | Description                                                                                            |
|-----------------|-------------|-------------|--------------------------------------------------------------------------------------------------------|
| Heating/Cooling | -1          | 1           | Whether to cool (values below 0) or to heat (values above 0). Intensity depends on the absolute value. |

## Preparing the Environment

To use the tool, quantifiable quality attributes must first be defined. In the case of the thermostat control these could be energy costs and user comfort. One way to quantify these attributes would be to define a fixed negative value when heating or cooling. This value would represent the energy costs. On the other hand, the current absolute temperature difference between current indoor temperature and the desired temperature could also be assigned as a negative value. This would be a dynamic measure of the current user satisfaction.

All individual quality attributes must be aggregated to a scalar value. For example, in the case above, this can be achieved by a simple sum of the two values. From a Reinforcement Learning perspective, the aggregated value represents the reward.

## Connecting the Tool

First the algorithm must be configured. This can be done by using the graphical user interface (pane: Configuration) or by POSTing the following JSON against the endpoint `/api/submit_configuration/`:

```
{
   "algorithm":"ppo",
   "action_space_specs":{
      "type":"continuous",
      "bounds":[
         ["Heating/Cooling", -1.0, 1.0]
      ]
   },
   "state_space_specs":{
      "type":"continuous",
      "bounds":[
            ["Indoor Temperature", -20, 50],
            ["Outdoor Temperature", -20, 50],
            ["Setpoint", -20, 50],
            ["Deviation from Setpoint", 0, 70],
            ["Occupancy", 0, 1]
      ]
   },
   "learning_rate":0.0003,
   "n_steps":2048,
   "batch_size":64,
   "n_epochs":10,
   "gamma":0.99,
   "gae_lambda":0.95,
   "clip_range":0.2,
   "clip_range_vf":null,
   "ent_coef":0.0,
   "vf_coef":0.5,
   "max_grad_norm":0.5,
   "use_sde":false,
   "sde_sample_freq":-1,
   "target_kl":null,
   "seed":null
}
```

Then the actual learning process can be started. For this purpose at each time step the state variables must be posted against the endpoint `/api/get_action/`. In addition, the reward, i.e. the aggregated, quantified quality attributes, must also be provided:

```
{
    "observation": [19.753, 4.363, 20.0, -0.246, 1.0],
    "reward": -0.13
}
```

The tool's response could look like this:

```
{
    "action": [-1.0]
}
```

The action returned by the tool must then be executed in the environment/simulation and the reward must be calculated based on the new state.

## Integrating Third Party Tools

The Online Learning tool can also be used to integrate and monitor third-party tools. Two endpoints serve this purpose: `/api/get_environmental_log_data/` and `/api/external_data/`.

The first endpoint can be used to query the current (and past) state and action variables as well as rewards. An exemplary query could look like this: `http://localhost:8000/api/get_environmental_log_data/?first_timestep=1500`. This query would fetch all the existing logs, starting from time step 1500. When there are more than 2500 logs after the provided time step, the query only returns the next 2500 logs. The response could like follows:

```
{
   "starting_from_timestep":1500,
   "observation_labels":[
      "Indoor Temperature",
      "Outdoor Temperature",
      "Setpoint",
      "Deviation from Setpoint",
      "Occupancy"
   ],
   "observations":[
      [20.6806583404541, 5.199999809265137, 20.0, 0.6806573867797852, 1.0],
      [20.39303970336914, 5.199999809265137, 20.0, 0.3930397927761078, 1.0],
      [20.1229248046875, 5.199999809265137, 20.0, 0.12292466312646866, 1.0],
      [19.869247436523438, 5.199999809265137, 20.0, -0.13075308501720428, 1.0],
      [19.770950317382812, 5.099999904632568, 20.0, -0.22904975712299347, 1.0]
   ],
   "action_labels":[
      "Heating/Cooling"
   ],
   "actions":[
      [-1.0],
      [-1.0],
      [-1.0],
      [-0.40311938524246216],
      [-1.0]
   ],
   "rewards":[
      -0.1,
      -0.05,
      -0.1,
      -0.01,
      0.0
   ],
   "algorithm_data_labels":[],
   "algorithm_data":[],
   "other_data_labels":[],
   "other_data":[]
}
```
Based on this data, further calculations can then be performed or additional tools can be connected. To show the results of these external calculations or tools in the monitoring pane of the Online Learning tool, the endpoint `/api/external_data/` can be utilized. It is done by POSTing a JSON with the following structure: 

```
{
    "unique_title": "Behavioral Drift Analysis",
    "labels": ["BDA Value"],
    "data": [
        [0.124],
        [0.237],
        [0.627],
        [0.544],
        [0.598]
    ]
}
```

If there is more than one variable, the `labels` list and the lists inside the `data` object can be populated with multiple values. For example:

```
{
    "unique_title": "Weather Forecast",
    "labels": ["Temperature Forecast", "Probability of Rain", "Humidity"],
    "data": [
        [20.5, 0.2, 0.2],
        [21.5, 0.1, 0.2],
        [22.0, 0.1, 0.3],
        [21.5, 0.3, 0.4],
        [19.0, 0.4, 0.6]
    ]
}
```

# Background
The Online Learning tool has been developed based on the conceptual work of paluno/UDE within WP3 (Task 3.1) of the ENACT - DevOps EU project ([https://www.enact-project.eu/](https://www.enact-project.eu/)). For further information concerning the theoretical background please have a look at the following publications:

 - Palm, A., Metzger, A., Pohl, K.: Online reinforcement learning for self-adaptive information systems. _In: Yu, E., Dustdar, S. (eds.) Int'l Conference on Advanced Information Systems Engineering (CAiSE 2020), Grenoble, France. LNCS, Springer (2020)._
 - Palm, Alexander, et al. "Towards Online Reinforcement Learning for Self-adaptive Fog Systems." _KuVS-Fachgespräch Fog Computing 2020: 8._
 - Ferry, Nicolas, et al. "Development and Operation of Trustworthy Smart IoT Systems: The ENACT Framework." _International Workshop on Software Engineering Aspects of Continuous Development and New Paradigms of Software Production and Deployment. Springer, Cham, 2019._