import React from "react";
import LinePlotCard from "./LinePlotCard";
const action_distribution_text = `In this diagram the evolution of the probability distribution computed by the policy
                                network is plotted. As the algorithm gets more confident on choosing certain parameter
                                values in certain context situations, the mean of the distribution should move toward a
                                certain value considered as optimal for the considered context situation and the
                                standard deviation of the distribution should converge towards zero. `;
const mean_standarddeviation_text = `In this diagram, the evolution of mean and standard deviation of the probability
                                distribution used for action sampling is plotted. As the algorithm gets more confident
                                on choosing certain parameter values in certain context situations, the mean of the
                                distribution should move toward a certain value considered as optimal for the considered
                                context situation and the standard deviation of the distribution should converge towards
                                zero. `;

class MonitoringAlgorithm extends React.Component {
    render() {
        return (
            <>
            </>
        );
    }
}

export default MonitoringAlgorithm;