import React from "react";
import LinePlotCard from "./LinePlotCard";
import {message} from "antd";
import backendUrl from "./BackendUrl";

class ExternalDataDiagram extends React.Component {
    state = {
        currentTimestep: 0,
        data: {},
        numberOfLogsAddedLastSync: null,
        catchingUp: true,
    }

    componentDidMount = () => {
        this.synchronizeDataFromBackend();
    }

    synchronizeDataFromBackend = () => {
        this.setState({catchingUp: this.state.numberOfLogsAddedLastSync === null || this.state.numberOfLogsAddedLastSync > 2000})

        if (this.state.catchingUp) {
            // we need to gather data faster to catch up
            this.pullNewDataFromBackend(() => {
                console.log("Synchronizing log data...");
                this.synchronizeDataFromBackend()
            });

        } else {
            let nextSync = Math.floor(Date.now()/10000)
            nextSync = (nextSync + 1) * 10 // round up to next 10 seconds
            let timeToNextSync = nextSync - Math.floor(Date.now()/1000)

            this.pullNewDataFromBackend(() => {
                setTimeout(() => {
                    console.log("Synchronizing log data...");
                    this.synchronizeDataFromBackend();
                }, timeToNextSync * 1000);
            });
        }
    }

    pullNewDataFromBackend = (onDone) => {
        let firstTimestep = this.state.currentTimestep;
        let context = this;

        fetch(backendUrl + '/api/external_data/?first_timestep=' + firstTimestep + '&unique_title=' + this.props.title,
            {
                method: 'get',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            })
            .then(response => {
                return response.json();
            }).then(json => {
                if ('error' in json) {
                    context.setState({catchingUp: false, numberOfLogsAddedLastSync: 0});
                    message.error(json['error']);
                } else {
                    this.processNewData(json);
                }

                onDone();
        }).catch(function (error) {
            context.setState({catchingUp: false, numberOfLogsAddedLastSync: 0});
            onDone();
        });
    }

    processNewData = (newData) => {
        let firstTimestepOfNewData = newData['starting_from_timestep'];
        let numberOfNewDataRows = newData['data'].length;

        this.setState({
            numberOfLogsAddedLastSync: numberOfNewDataRows,
            currentTimestep: firstTimestepOfNewData + numberOfNewDataRows
        })

        let data = this.convertAPIData(newData['labels'], newData['data']);
        for (let [key, value] of Object.entries(this.state.data)) {
            if (value.length !== firstTimestepOfNewData) {
                this.invalidateAllData();
                return;
            }
        }

        this.appendDataToState(data, this.state.data, firstTimestepOfNewData);
        this.forceUpdate();
    }

    appendDataToState = (newData, dataObject, firstTimestepOfNewData) => {
        for (const [key, value] of Object.entries(newData)) {
            if (!(key in dataObject))
                dataObject[key] = [];

            // sanity check that dataset is in sync
            if (dataObject[key].length !== firstTimestepOfNewData) {
                this.invalidateAllData();
                return;
            }

            dataObject[key].push(...value);
        }
    }

    invalidateAllData = () => {
        this.setState({
            currentTimestep: 0,
            data: {},
            numberOfLogsAddedLastSync: null,
            catchingUp: true
        })
    }

    convertAPIData = (labels, data) => {
        // format from API is on list of labels and a nested list like a table
        // here we extract each column of the table to have a POJO with table header as key and the whole data as list
        let convertedData = {};
        labels.forEach((label, index) => {
            convertedData[label] = data.map(function (row) {
                return row[index];
            });
        });

        return convertedData;
    }

    render = () => {
        return (
            <LinePlotCard
                title={this.props.title}
                description={this.props.description}
                data={this.state.data}
                catchingUp={this.state.catchingUp}
                currentlyShown={this.props.currentlyShown}
                annotations={this.props.annotations}
            />
        );
    }
}

export default ExternalDataDiagram;