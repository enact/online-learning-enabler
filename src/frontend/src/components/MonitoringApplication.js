import React from "react";
import LinePlotCard from "./LinePlotCard";
import {message} from "antd";
import ExternalDataDiagram from "./ExternalDataDiagram";
import backendUrl from "./BackendUrl";

const observation_text = `In this diagram the progression of the observation variables is shown. To zoom into the diagram, a rectangular selection can be made. The "Moving Average Interval" slider can be used to smooth the diagram. The slider "Granularity" can be used to change the number of points drawn. Be careful with the latter slider, however, as high values will greatly affect the performance of the website.`;
const action_text = `In this diagram the selected actions are shown. To zoom into the diagram, a rectangular selection can be made. The "Moving Average Interval" slider can be used to smooth the diagram. The slider "Granularity" can be used to change the number of points drawn. Be careful with the latter slider, however, as high values will greatly affect the performance of the website.`;
const reward_text = `In this diagram the progression of the reward value is shown. To zoom into the diagram, a rectangular selection can be made. The "Moving Average Interval "slider can be used to smooth the diagram. The slider "Granularity" can be used to change the number of points drawn. Be careful with the latter slider, however, as high values will greatly affect the performance of the website.`;

class MonitoringApplication extends React.Component {
    state = {
        currentTimestep: 0,
        observationData: {},
        actionData: {},
        rewardData: [],
        algorithmData: {},
        otherData: {},
        numberOfLogsAddedLastSync: null,
        catchingUp: true,
        externalDataIds: [],
        annotations: []
    }

    componentDidMount = () => {
        this.synchronizeDataFromBackend();
    }

    synchronizeDataFromBackend = () => {
        this.setState({catchingUp: this.state.numberOfLogsAddedLastSync === null || this.state.numberOfLogsAddedLastSync > 2000})

        if (this.state.catchingUp) {
            // we need to gather data faster to catch up
            this.pullNewDataFromBackend(() => {
                console.log("Synchronizing log data...");
                this.synchronizeDataFromBackend()
            });

        } else {
            let nextSync = Math.floor(Date.now()/10000)
            nextSync = (nextSync + 1) * 10 // round up to next 10 seconds
            let timeToNextSync = nextSync - Math.floor(Date.now()/1000)

            this.pullNewDataFromBackend(() => {
                setTimeout(() => {
                    console.log("Synchronizing log data...");
                    this.synchronizeDataFromBackend();
                }, timeToNextSync * 1000);
            });
        }
    }

    pullNewDataFromBackend = (onDone) => {
        let firstTimestep = this.state.currentTimestep;
        let context = this;

        fetch(backendUrl + '/api/get_environmental_log_data/?first_timestep=' + firstTimestep,
            {
                method: 'get',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            })
            .then(response => {
                return response.json();
            }).then(json => {
                if ('error' in json) {
                    context.setState({catchingUp: false, numberOfLogsAddedLastSync: 0});
                    message.error(json['error']);
                } else {
                    this.processNewData(json);
                }

                onDone();
        }).catch(function (error) {
            context.setState({catchingUp: false, numberOfLogsAddedLastSync: 0});
            onDone();
        });

        fetch(backendUrl + '/api/get_available_external_log_data_ids/',
            {
                method: 'get',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            })
            .then(response => {
                return response.json();
            }).then(json => {
                if ('error' in json) {
                    message.error(json['error']);
                } else {
                    context.setState({externalDataIds: json['external_data_ids']});
                }
        })

        fetch(backendUrl + '/api/annotations/',
            {
                method: 'get',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            })
            .then(response => {
                return response.json();
            }).then(json => {
                if ('error' in json) {
                    message.error(json['error']);
                } else {
                    if (!LinePlotCard.deepEqual(json['annotations'], this.state.annotations))
                        context.setState({annotations: json['annotations']});
                }
        })
    }

    processNewData = (newData) => {
        let firstTimestepOfNewData = newData['starting_from_timestep'];
        let numberOfNewDataRows = newData['rewards'].length;

        this.setState({
            numberOfLogsAddedLastSync: numberOfNewDataRows,
            currentTimestep: firstTimestepOfNewData + numberOfNewDataRows
        })

        let observationData = this.convertAPIData(newData['observation_labels'], newData['observations']);
        let actionData = this.convertAPIData(newData['action_labels'], newData['actions']);
        let rewardData = newData['rewards'];
        let algorithmData = this.convertAPIData(newData['algorithm_data_labels'], newData['algorithm_data']);
        let otherData = this.convertAPIData(newData['other_data_labels'], newData['other_data']);

        if (this.state.rewardData.length !== firstTimestepOfNewData) {
            this.invalidateAllData();
            return;
        }

        this.state.rewardData.push(...rewardData);
        this.appendDataToState(observationData, this.state.observationData, firstTimestepOfNewData);
        this.appendDataToState(actionData, this.state.actionData, firstTimestepOfNewData);
        this.appendDataToState(algorithmData, this.state.algorithmData, firstTimestepOfNewData);
        this.appendDataToState(otherData, this.state.otherData, firstTimestepOfNewData);

        this.forceUpdate();
    }

    appendDataToState = (newData, dataObject, firstTimestepOfNewData) => {
        for (const [key, value] of Object.entries(newData)) {
            if (!(key in dataObject))
                dataObject[key] = [];

            // sanity check that dataset is in sync
            if (dataObject[key].length !== firstTimestepOfNewData) {
                this.invalidateAllData();
                return;
            }

            dataObject[key].push(...value);
        }
    }

    invalidateAllData = () => {
        this.setState({
            currentTimestep: 0,
            observationData: {},
            actionData: {},
            rewardData: [],
            algorithmData: {},
            otherData: {},
            numberOfLogsAddedLastSync: null,
            catchingUp: true
        })
    }

    convertAPIData = (labels, data) => {
        // format from API is on list of labels and a nested list like a table
        // here we extract each column of the table to have a POJO with table header as key and the whole data as list
        let convertedData = {};
        labels.forEach((label, index) => {
            convertedData[label] = data.map(function (row) {
                return row[index];
            });
        });

        return convertedData;
    }

    render = () => {
        return (
            <>
                <LinePlotCard
                    title='Observations'
                    description={observation_text}
                    data={this.state.observationData}
                    catchingUp={this.state.catchingUp}
                    currentlyShown={this.props.currentlyOpen}
                    annotations={this.state.annotations}
                />
                <LinePlotCard
                    title='Actions'
                    description={action_text}
                    data={this.state.actionData}
                    catchingUp={this.state.catchingUp}
                    currentlyShown={this.props.currentlyOpen}
                    annotations={this.state.annotations}
                />
                <LinePlotCard
                    title='Rewards'
                    description={reward_text}
                    data={{reward: this.state.rewardData}}
                    catchingUp={this.state.catchingUp}
                    currentlyShown={this.props.currentlyOpen}
                    annotations={this.state.annotations}
                />
                {this.state.externalDataIds.map((dataId) => (
                    <ExternalDataDiagram
                        key={dataId}
                        title={dataId}
                        currentlyShown={this.props.currentlyOpen}
                        description=''
                        annotations={this.state.annotations}
                    />
                ))}
            </>
        );
    }
}

export default MonitoringApplication;