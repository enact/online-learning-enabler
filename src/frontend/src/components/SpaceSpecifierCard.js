import React from "react";
import {Card, Typography, Radio, InputNumber, Input} from 'antd';
const {Paragraph, Text} = Typography;

class SpaceSpecifierCard extends React.Component {
    constructor(props) {
        super(props);
        this.state.title = props.title
        this.state.description = props.description
        this.chartRef = React.createRef();
        this.count = 0;
        this.disableContinuous = false;

        if ('disableContinuous' in this.props)
            this.disableContinuous = props.disableContinuous
    }

    state = {
        space_type: '',
        number_of_discrete_values: 2,
        number_of_continuous_dimensions: 2,
        ids_of_continuous_bounds_forms: [0, 1]
    };

    static getDerivedStateFromProps = (props, state) => {
        if (props.currentSpaceSpec === null)
            return state;

        state.space_type = props.currentSpaceSpec.type;
        if (props.currentSpaceSpec.type === 'continuous') {
            state.number_of_continuous_dimensions = props.currentSpaceSpec.bounds.length;
            state.ids_of_continuous_bounds_forms = Array.from(Array(state.number_of_continuous_dimensions).keys());

            props.currentSpaceSpec.bounds.forEach((value, i) => {
                state['label-' + i] = value[0]
                state['lower-' + i] = value[1]
                state['upper-' + i] = value[2]
            });
        } else if (props.currentSpaceSpec.type === 'discrete') {
            state.number_of_discrete_values = props.currentSpaceSpec.number_of_discrete_values;
        }

        return state
    }

    submitDataToParent = () => {
        let data = {
            type: this.state.space_type
        }

        if (this.state.space_type === 'discrete') {
            data['number_of_discrete_values'] = this.state.number_of_discrete_values;
        } else if (this.state.space_type === 'continuous') {
            let bounds = []
            this.state.ids_of_continuous_bounds_forms.forEach((value, index) => {
                let label = 'Dimension ' + value;
                let lower_bound = '-inf';
                let upper_bound = 'inf';

                if ('label-' + value in this.state)
                    label = this.state['label-' + value]

                if ('lower-' + value in this.state)
                    lower_bound = this.state['lower-' + value]

                if ('upper-' + value in this.state)
                    upper_bound = this.state['upper-' + value]

                bounds.push([label, lower_bound, upper_bound])
            });

            data['number_of_continuous_dimensions'] = this.state.number_of_continuous_dimensions;
            data['bounds'] = bounds
        } else {
            return; // no selection made yet
        }

        this.props.parentCallback(data);
    }

    onSpaceTypeChanged = e => {
        this.state.space_type = e.target.value;
        this.submitDataToParent();
    };

    numberOfDiscreteValuesChanged = (value) => {
        this.state.number_of_discrete_values = value;
        this.submitDataToParent();
    }

    numberOfContinuousDimensionsChanged = (value) => {
        this.state.number_of_continuous_dimensions = value;
        this.state.ids_of_continuous_bounds_forms = Array.from(Array(value).keys());
        this.submitDataToParent();
    }

    onChange = (e) => {
        if (e.target.name.includes('label-') && e.target.value === '') {
            this.state[e.target.name] = e.target.placeholder;
        } else if (e.target.name.includes('upper-') && e.target.value === '') {
            this.state[e.target.name] = 'inf';
        } else if (e.target.name.includes('lower-') && e.target.value === '') {
            this.state[e.target.name] = '-inf';
        } else {
            this.state[e.target.name] = e.target.value;
        }

        this.submitDataToParent();
    }

    render() {
        return (
            <>
                <Paragraph>
                    <Card headStyle={{fontWeight: 'bold', fontSize: '200%'}} title={this.state.title}>
                        <Radio.Group onChange={this.onSpaceTypeChanged} value={this.state.space_type}>
                            <Radio.Button value={'discrete'}>Discrete</Radio.Button>
                            <Radio.Button value={'continuous'} style={{marginLeft: '1px'}} disabled={this.disableContinuous}>Continuous</Radio.Button>
                        </Radio.Group>
                        {this.state.space_type === 'discrete' ?
                            <div style={{marginTop: '20px'}}><Text>Number of possible values:</Text><InputNumber min={2} defaultValue={2} value={this.state.number_of_discrete_values} onChange={this.numberOfDiscreteValuesChanged} style={{marginLeft: '20px'}} /></div>

                            : this.state.space_type === 'continuous' ?
                                <>
                                    <div style={{marginTop: '20px', marginBottom: '20px'}}><Text>Number of dimensions:</Text><InputNumber min={1} defaultValue={1} value={this.state.number_of_continuous_dimensions} onChange={this.numberOfContinuousDimensionsChanged} style={{marginLeft: '20px'}}/></div>
                                    {
                                        this.state.ids_of_continuous_bounds_forms.map(input =>
                                            <div key={'wrapper-' + input}>
                                                Label: <Input placeholder={'Dimension ' + input} onChange={this.onChange} value={this.state['label-' + input]} key={'label-' + input} name={'label-' + input} style={{maxWidth: '200px', marginLeft: '20px', marginRight: '20px'}}/>
                                                Lower Bound: <Input placeholder={'-inf'} onChange={this.onChange} value={this.state['lower-' + input]} key={'lower-' + input} name={'lower-' + input} style={{maxWidth: '80px', marginLeft: '20px', marginRight: '20px'}}/>
                                                Upper Bound: <Input placeholder={'inf'} onChange={this.onChange} value={this.state['upper-' + input]} key={'upper-' + input} name={'upper-' + input} style={{maxWidth: '80px', marginLeft: '20px', marginRight: '20px'}}/>
                                            </div>
                                        )
                                    }

                                    <ul style={{marginTop: '20px'}}>
                                        <li>
                                            <Text>For each dimensions an upper and lower bound has to be specified</Text>
                                        </li>
                                        <li>
                                            <Text><b>-inf</b> and <b>inf</b> can be used for unbounded intervals</Text>
                                        </li>
                                    </ul>
                                </>

                                :
                                <ul style={{marginTop: '20px'}}>
                                    <li>
                                        <Text>{ this.state.description }</Text>
                                    </li>
                                </ul>
                        }
                    </Card>
                </Paragraph>
            </>
        );
    }
}

export default SpaceSpecifierCard;