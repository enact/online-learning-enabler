import React from "react";
import {Card, Typography, Input, Button, Popconfirm, message, Upload, Radio} from 'antd';
import {UploadOutlined, DownloadOutlined} from '@ant-design/icons';
import SpaceSpecifierCard from "./SpaceSpecifierCard";
import backendUrl from "./BackendUrl";

const { Paragraph, Text} = Typography;

class ConfigurationPPO extends React.Component {
    hiddenLinkForDownload = React.createRef();
    state = {
        learning_rate: 3e-4,
        n_steps: 2048,
        batch_size: 64,
        n_epochs: 10,
        gamma: 0.99,
        gae_lambda: 0.95,
        clip_range: 0.2,
        clip_range_vf: null,
        ent_coef: 0.0,
        vf_coef: 0.5,
        max_grad_norm: 0.5,
        use_sde: null,
        sde_sample_freq: -1,
        target_kl: null,
        seed: null,
        currentlyUploading: false,
        currentActionSpaceSpec: null,
        currentStateSpaceSpec: null,
    };

    confirmUpdate = () => {
        if (!('currentStateSpaceSpec' in this.state) || !('currentActionSpaceSpec' in this.state)) {
            message.error('State and action space have to be specified!')
            return
        }

        let data = {
            algorithm: 'ppo',
            state_space_data: this.state.currentStateSpaceSpec,
            action_space_data: this.state.currentActionSpaceSpec,

            learning_rate: this.state.learning_rate,
            n_steps: this.state.n_steps,
            batch_size: this.state.batch_size,
            n_epochs: this.state.n_epochs,
            gamma: this.state.gamma,
            gae_lambda: this.state.gae_lambda,
            clip_range: this.state.clip_range,
            clip_range_vf: this.state.clip_range_vf,
            ent_coef: this.state.ent_coef,
            vf_coef: this.state.vf_coef,
            max_grad_norm: this.state.max_grad_norm,
            use_sde: this.state.use_sde,
            sde_sample_freq: this.state.sde_sample_freq,
            target_kl: this.state.target_kl,
        }

        if (this.state.seed !== null)
            data.seed = this.state.seed

        fetch(backendUrl + '/api/submit_configuration/',
            {
                method: 'post',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            })
            .then(response => {
                return response.json();
            }).then(json => {
                if ('error' in json) {
                    message.error(json['error']);
                } else {
                    message.success('Restarted Learning Process!');
                }
            }).catch(function (error) {
                message.error('An error occurred!')
            });
    }
    
    onChange = (e) => {
        this.setState({[e.target.name]: e.target.value});
    }

    getStateSpaceInformation = (data) => {
        this.setState({currentStateSpaceSpec: data});
    }

    getActionSpaceInformation = (data) => {
        this.setState({currentActionSpaceSpec: data});
    }

    onUploadPressed = (file) => {
        this.setState({currentlyUploading: true});

        let context = this;
        let reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => {
            fetch(backendUrl + '/api/upload_model/',
                {
                    method: 'post',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        algorithm: 'ppo',
                        base_64_string: reader.result.split(',').pop()
                    })

                })
                .then(response => {
                    context.setState({currentlyUploading: false});
                    return response.json();
                }).then(json => {
                    if ('error' in json) {
                        message.error(json['error']);
                    } else {
                        message.success('Model loaded successfully!');
                        context.setState({
                            currentActionSpaceSpec: json.action_space_specs,
                            currentStateSpaceSpec: json.state_space_specs,
                            gamma: json.gamma,
                            n_steps: json.n_steps,
                            ent_coef: json.ent_coef,
                            learning_rate: json.learning_rate,
                            vf_coef: json.vf_coef,
                            max_grad_norm: json.max_grad_norm,
                            lam: json.lam,
                            nminibatches: json.nminibatches,
                            noptepochs: json.noptepochs,
                            cliprange: json.cliprange,
                            seed: json.seed
                        });
                    }
            }).catch(function (error) {
                message.error('An error occurred!')
            });
        };

        reader.onerror = (error) => {
            context.setState({currentlyUploading: false});
            message.error("Error parsing file!")
        };
    };

    onDownloadClicked = () => {
        fetch(backendUrl + '/api/download_model/')
            .then(resp => resp.blob())
            .then(blob => {
                const url = window.URL.createObjectURL(blob);
                const a = this.hiddenLinkForDownload.current;
                a.href = url;
                a.download = 'ppo2_model.zip';
                a.click();
                a.href = ''
            })
            .catch((error) => {
                console.log(error);
                message.error("An error occurred!")
            });
    }
    
    render() {
        return (
            <>
                <Paragraph>
                    <Card headStyle={{fontWeight: 'bold', fontSize: '200%'}} title={"Load Pretrained Model"}>
                        <div style={{display: 'flex'}}>
                            <Upload beforeUpload={this.onUploadPressed} showUploadList={false} accept={".zip"}>
                                <Button icon={<UploadOutlined/>} loading={this.state.currentlyUploading}>
                                    Upload Pretrained Model
                                </Button>
                            </Upload>
                            <Button style={{marginLeft: '20px'}} onClick={this.onDownloadClicked}>
                                <DownloadOutlined/> Download Current Model
                            </Button>
                            <a style={{display: 'hidden'}} ref={this.hiddenLinkForDownload}/>
                        </div>
                        <ul style={{marginTop: '20px'}}>
                            <li>
                                <Text>Upload a .zip file that was created by <b>stable-baselines</b> with the method <b>PPO2.save()</b>.</Text>
                            </li>
                            <li>
                                <Text>Wait until the configuration is updated below.</Text>
                            </li>
                        </ul>
                    </Card>
                </Paragraph>
                <SpaceSpecifierCard currentSpaceSpec={this.state.currentStateSpaceSpec} title='State Space' description='Is the agent supposed to have discrete or continuous values as inputs?' parentCallback={this.getStateSpaceInformation}/>
                <SpaceSpecifierCard currentSpaceSpec={this.state.currentActionSpaceSpec} title='Action Space' description='Is the agent supposed to output discrete or continuous values?' parentCallback={this.getActionSpaceInformation}/>
                <Paragraph>
                    <Card headStyle={{fontWeight: 'bold', fontSize: '200%'}} title={"Hyperparameter"}>
                        <Text><b>Learning Rate (float)</b></Text><br/>
                        <Input value={this.state.learning_rate} onChange={this.onChange} name='learning_rate' style={{marginTop: '10px', marginBottom: '20px', maxWidth: '500px'}}/>
                        <br/><Text><b>N-Steps: The number of steps to run for each environment per update (int)</b></Text><br/>
                        <Input value={this.state.n_steps} onChange={this.onChange} name='n_steps' style={{marginTop: '10px', marginBottom: '20px', maxWidth: '500px'}}/>
                        <br/><Text><b>Minibatch size (int)</b></Text><br/>
                        <Input value={this.state.batch_size} onChange={this.onChange} name='batch_size' style={{marginTop: '10px', marginBottom: '20px', maxWidth: '500px'}}/>
                        <br/><Text><b>N Epochs: Number of epochs when optimizing the surrogate loss (int)</b></Text><br/>
                        <Input value={this.state.n_epochs} onChange={this.onChange} name='n_epochs' style={{marginTop: '10px', marginBottom: '20px', maxWidth: '500px'}}/>
                        <br/><Text><b>Gamma: Discount factor (float)</b></Text><br/>
                        <Input value={this.state.gamma} onChange={this.onChange} name='gamma' style={{marginTop: '10px', marginBottom: '20px', maxWidth: '500px'}}/>
                        <br/><Text><b>GAE-Lambda: Factor for trade-off of bias vs variance for Generalized Advantage Estimator (float)</b></Text><br/>
                        <Input value={this.state.gae_lambda} onChange={this.onChange} name='gae_lambda' style={{marginTop: '10px', marginBottom: '20px', maxWidth: '500px'}}/>
                        <br/><Text><b>Clip Range (float)</b></Text><br/>
                        <Input value={this.state.clip_range} onChange={this.onChange} name='clip_range' style={{marginTop: '10px', marginBottom: '20px', maxWidth: '500px'}}/>
                        <br/><Text><b>Clipping range for the value function (float|null)</b></Text><br/>
                        <Input value={this.state.clip_range_vf} onChange={this.onChange} name='clip_range_vf' style={{marginTop: '10px', marginBottom: '20px', maxWidth: '500px'}}/>
                        <br/><Text><b>Entropy coefficient for the loss calculation (float)</b></Text><br/>
                        <Input value={this.state.ent_coef} onChange={this.onChange} name='ent_coef' style={{marginTop: '10px', marginBottom: '20px', maxWidth: '500px'}}/>
                        <br/><Text><b>Value function coefficient for the loss calculation (float)</b></Text><br/>
                        <Input value={this.state.vf_coef} onChange={this.onChange} name='vf_coef' style={{marginTop: '10px', marginBottom: '20px', maxWidth: '500px'}}/>
                        <br/><Text><b>The maximum value for the gradient clipping (float)</b></Text><br/>
                        <Input value={this.state.max_grad_norm} onChange={this.onChange} name='max_grad_norm' style={{marginTop: '10px', marginBottom: '20px', maxWidth: '500px'}}/>
                        <br/><Text><b>Use Generalized State Dependent Exploration (true|false)</b></Text><br/>
                        <Input value={this.state.use_sde} onChange={this.onChange} name='use_sde' style={{marginTop: '10px', marginBottom: '20px', maxWidth: '500px'}}/>
                        <br/><Text><b>Sample noise matrix every N steps when using gSDE (int)</b></Text><br/>
                        <Input value={this.state.sde_sample_freq} onChange={this.onChange} name='sde_sample_freq' style={{marginTop: '10px', marginBottom: '20px', maxWidth: '500px'}}/>
                        <br/><Text><b>Limit the KL divergence between updates (float|null)</b></Text><br/>
                        <Input value={this.state.target_kl} onChange={this.onChange} name='target_kl' style={{marginTop: '10px', marginBottom: '20px', maxWidth: '500px'}}/>
                        <br/><Text><b>Seed</b></Text><br/>
                        <Input value={this.state.seed} onChange={this.onChange} name='seed' style={{marginTop: '10px', marginBottom: '20px', maxWidth: '500px'}}/>
                    </Card>
                </Paragraph>
                <Popconfirm
                    title="This will reset the learning process. Continue?"
                    onConfirm={this.confirmUpdate}
                    okText="Yes"
                    cancelText="Abort"
                >
                    <Button type="primary">
                        Update Configuration and Restart Learning
                    </Button>
                </Popconfirm>
            </>
        );
    }
}

export default ConfigurationPPO;