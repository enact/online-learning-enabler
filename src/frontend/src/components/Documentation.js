import React from "react";
import Markdown from "react-markdown/with-html";
import gfm from "remark-gfm";
import {Card, Typography} from "antd";
import 'github-markdown-css';

const {Paragraph} = Typography;


class Documentation extends React.Component {
    state = {
        doc_text: null,
    };

    constructor() {
        super();
        fetch('/static/documentation.md')
            .then((r) => r.text())
            .then(text => {
                this.handleChange(text);
            });
    }

    handleChange = doc_text => {
      this.setState({ doc_text });
    };

    render() {
        const {doc_text} = this.state;
        return (
            <Paragraph>
                <Card headStyle={{fontWeight: 'bold', fontSize: '200%'}}>
                    <div className='markdown-body'>
                        <Markdown plugins={[gfm]} source={doc_text} escapeHtml={false} />
                    </div>
                </Card>
            </Paragraph>
        );
    }
}

export default Documentation;