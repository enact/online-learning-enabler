import React from "react";
import { Card, Typography, Collapse, Spin, Slider, InputNumber, Row, Col } from 'antd';
import {Chart} from "@antv/g2";
const { Panel } = Collapse;
const { Paragraph } = Typography;


// https://reactjs.org/docs/react-component.html
// https://www.createwithdata.com/react-chartjs-dashboard/
class LinePlotCard extends React.Component {
    state = {
        showSpinner: true,
        movingAverageInterval: 100,
        movingAverageIntervalTemporary: 100,
        maxNumberOfPlottingPoints: 200,
        maxNumberOfPlottingPointsTemporary: 200,
    };

    constructor(props) {
        super(props);
        this.chartRef = React.createRef();
        this.currentTimestep = 0;
        this.drawnAnnotations = [];
    }

    componentDidMount() {
        this.chart = new Chart({
            container: this.chartRef.current,
            autoFit: true,
            height: 500
        });

        this.chart.line().position('timestep*value').color('value_name');

        // display when hovering with mouse over the chart
        this.chart.tooltip({
            showCrosshairs: true,
            shared: true,
        });

        // zooming function
        this.chart.interaction('brush');
        this.drawAllNewAnnotations();

        this.chart.render();
        this.chart.forceFit();
    }

    componentDidUpdate(prevProps, prevState) {
        if (!prevProps.currentlyShown && this.props.currentlyShown)
            this.chart.forceFit();

        if (this.chart.getData().length === 0 && !this.state.showSpinner)
            this.setState({showSpinner: true});

        if (this.props.catchingUp || Object.keys(this.props.data).length === 0)
            return;

        // update chart
        let dataPoints = [];
        for (const [label, array] of Object.entries(this.props.data)) {
            if (this.currentTimestep === this.props.data[label].length &&
                !prevProps.catchingUp &&
                prevState.maxNumberOfPlottingPoints === this.state.maxNumberOfPlottingPoints &&
                prevState.movingAverageInterval === this.state.movingAverageInterval &&
                LinePlotCard.deepEqual(prevProps.annotations, this.props.annotations))
                return;  // nothing changed since last update, prevent re-rendering

            let drawEveryNthPoint = Math.floor(array.length / this.state.maxNumberOfPlottingPoints);
            array.forEach((entry, index) => {
                if (entry !== null && index % drawEveryNthPoint === 0 && index > this.state.movingAverageInterval) {
                    let averagedValue = LinePlotCard.average(array.slice(index - this.state.movingAverageInterval, index + 1))
                    dataPoints.push({timestep: index, value: averagedValue, value_name: label})
                }
            });
        }

        this.currentTimestep = this.props.data[Object.keys(this.props.data)[0]].length;

        this.chart.changeData(dataPoints);

        this.drawAllNewAnnotations()
        this.chart.render();
        this.chart.forceFit();

        if (this.chart.getData().length !== 0 && this.state.showSpinner)
            this.setState({showSpinner: false});
    }

    drawAllNewAnnotations() {
        this.props.annotations.forEach((annotation => {
            if (this.drawnAnnotations.some((drawnAnnotation) => { return LinePlotCard.deepEqual(drawnAnnotation, annotation) }))
                return;  // Is this annotation already drawn?

            LinePlotCard.drawAnnotation(this.chart, annotation.timestep, annotation.text)
            this.drawnAnnotations.push(annotation);
        }));
    }

    static drawAnnotation(chart, timestep, text) {
        chart.annotation().dataMarker({
            position: [timestep, 'min'],
            direction: 'upward',
            autoAdjust: false,
            text: {
                content: text,
                style: {
                    textAlign: 'center',
                    fontSize: 14
                },
            },
            line: {
                length: 441,
                style: {
                    lineWidth: 3,
                },
            },
        });
    }

    static deepEqual(x, y) {
        const ok = Object.keys, tx = typeof x, ty = typeof y;
        return x && y && tx === 'object' && tx === ty ? (
            ok(x).length === ok(y).length &&
            ok(x).every(key => LinePlotCard.deepEqual(x[key], y[key]))
        ) : (x === y);
    }

    static average(array) {
        return array.reduce((a, b) => a + b, 0) / array.length;
    }

    static movingAverage(array, n) {
        // calculate average for subarray
        let avg = arr => arr.reduce((a, b) => a + b, 0) / arr.length;
        let result = [];

        // pad beginning of result with null values
        for (let i = 0; i < n - 1; i++)
            result.push(null);

        // calculate average for each subarray and add to result
        for (let i = 0, len = array.length - n; i <= len; i++)
            result.push(avg(array.slice(i, i + n)));

        return result;
    }

    onChangeMovingAverageInterval = (newMovingAverageInterval) => {
        this.setState({movingAverageIntervalTemporary: newMovingAverageInterval});
    }

    onAfterChangeMovingAverageInterval = (newMovingAverageInterval) => {
        this.setState({movingAverageInterval: newMovingAverageInterval});
    }

    onChangeMaxNumberOfPlottingPoints = (newMaxNumberOfPlottingPoints) => {
        this.setState({maxNumberOfPlottingPointsTemporary: newMaxNumberOfPlottingPoints});
    }

    onAfterChangeMaxNumberOfPlottingPoints = (newMaxNumberOfPlottingPoints) => {
        this.setState({maxNumberOfPlottingPoints: newMaxNumberOfPlottingPoints});
    }

    render() {
        return (
            <>
                <Paragraph>
                    <Card headStyle={{fontWeight: 'bold', fontSize: '200%'}} title={this.props.title||"No title defined"}>
                        <div ref={this.chartRef} style={{width: 'inherit'}}/>

                        <Row justify={"space-around"}>
                            <Col style={{marginTop: '30px', width:"50%"}}>
                                <Row style={{marginLeft: "5px"}}>
                                    <span>Moving Average Interval</span>
                                </Row>
                                <Row style={{flexFlow: "row"}}>
                                    <Col span={12}>
                                        <Slider
                                            min={0}
                                            max={1000}
                                            onChange={this.onChangeMovingAverageInterval}
                                            onAfterChange={this.onAfterChangeMovingAverageInterval}
                                            value={typeof this.state.movingAverageIntervalTemporary === 'number' ? this.state.movingAverageIntervalTemporary : 0}
                                        />
                                    </Col>
                                    <Col span={12}>
                                        <InputNumber
                                            min={0}
                                            step={1000}
                                            style={{margin: '0 16px'}}
                                            value={this.state.movingAverageIntervalTemporary}
                                            onChange={(newMovingAverageInterval) => {
                                                this.onChangeMovingAverageInterval(newMovingAverageInterval);
                                                this.onAfterChangeMovingAverageInterval(newMovingAverageInterval)
                                            }}
                                        />
                                    </Col>
                                </Row>
                            </Col>

                            <Col style={{marginTop: '30px', width: "50%"}}>
                                <Row style={{marginLeft: "5px"}}>
                                    <span>Granularity <b>(impacts performance!)</b></span>
                                </Row>
                                <Row style={{flexFlow: "row"}}>
                                    <Col span={12}>
                                        <Slider
                                            min={10}
                                            max={1000}
                                            onChange={this.onChangeMaxNumberOfPlottingPoints}
                                            onAfterChange={this.onAfterChangeMaxNumberOfPlottingPoints}
                                            value={typeof this.state.maxNumberOfPlottingPointsTemporary === 'number' ? this.state.maxNumberOfPlottingPointsTemporary : 0}
                                        />
                                    </Col>
                                    <Col span={12}>
                                        <InputNumber
                                            min={10}
                                            style={{margin: '0 16px'}}
                                            value={this.state.maxNumberOfPlottingPointsTemporary}
                                            onChange={(newMaxNumberOfPlottingPoints) => {
                                                this.onChangeMaxNumberOfPlottingPoints(newMaxNumberOfPlottingPoints);
                                                this.onAfterChangeMaxNumberOfPlottingPoints(newMaxNumberOfPlottingPoints)
                                            }}
                                        />
                                    </Col>
                                </Row>
                            </Col>
                        </Row>

                        {this.state.showSpinner ?
                            <Spin size="large" style={{position: 'absolute', left: '50%', bottom: '50%'}}/> : ''}
                    </Card>
                    <Collapse defaultActiveKey={['1']}>
                            <Panel header="Show/Hide Details" key="0">
                                <p>{this.props.description||"No description defined"}</p>
                            </Panel>
                    </Collapse>
                </Paragraph>
            </>
        );
    }
}

export default LinePlotCard;