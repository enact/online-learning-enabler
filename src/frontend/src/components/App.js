import React from "react";
import {Layout, Menu} from "antd";
import {EyeFilled, CaretDownOutlined, SettingFilled, BookFilled } from '@ant-design/icons';
import Documentation from "./Documentation";
import Configuration from "./Configuration";
import MonitoringAlgorithm from "./MonitoringAlgorithm";
import MonitoringApplication from "./MonitoringApplication";


const {Header, Footer} = Layout;
const SubMenu = Menu.SubMenu;

class App extends React.Component {
    state = {
        documentationOpen: true,
        configurationOpen: false,
        monitorAlgorithmOpen: false,
        monitorApplicationOpen: false,
    }

    showDocumentation = () => {
        this.hideAll();
        this.setState({documentationOpen: true});
        document.getElementById("documentation").style.display = "block";
    };

    showConfiguration = () => {
        this.hideAll();
        this.setState({configurationOpen: true});
        document.getElementById("configuration").style.display = "block";
    };

    showMonitoringAlgorithm = () => {
        this.hideAll();
        this.setState({monitorAlgorithmOpen: true});
        document.getElementById("monitoring-algorithm").style.display = "block";
    };

    showMonitoringApplication = () => {
        this.hideAll();
        this.setState({monitorApplicationOpen: true});
        document.getElementById("monitoring-application").style.display = "block";
    };

    hideAll = () => {
        document.getElementById("documentation").style.display = "none";
        document.getElementById("monitoring-application").style.display = "none";
        document.getElementById("monitoring-algorithm").style.display = "none";
        document.getElementById("configuration").style.display = "none";

        this.setState({
            documentationOpen: false,
            configurationOpen: false,
            monitorAlgorithmOpen: false,
            monitorApplicationOpen: false,
        });

        window.scrollTo(0, 0);
    };

    render() {
        return (
            <Layout style={{minHeight: "100vh"}}>
                <Header style={{background: "#fff", padding: "10px", height: "130px", position: 'fixed', zIndex: 1, width: '100%'}}>
                    <Menu selectable={false} theme="light" mode="horizontal" style={{lineHeight: "115px"}}>
                        <Menu.Item key="logo">
                            <a href="https://www.enact-project.eu/">
                                <img
                                    style={{height: "80px"}}
                                    src="/static/paluno_logo.png"
                                    alt="logo enact"
                                />
                            </a>
                        </Menu.Item>
                        <Menu.Item onClick={this.showDocumentation}>
                            <span className="menu-item"><BookFilled/><span style={{paddingLeft: 10}}>Documentation </span></span>
                        </Menu.Item>
                        <Menu.Item onClick={this.showMonitoringApplication}>
                            <span className="menu-item"><EyeFilled/><span style={{paddingLeft: 10}}>Monitoring </span></span>
                        </Menu.Item>
                        <Menu.Item onClick={this.showConfiguration}>
                            <span className="menu-item"><span style={{paddingLeft: 10}}><SettingFilled/>Configuration </span></span>
                        </Menu.Item>
                    </Menu>
                </Header>
                <div className="outer-wrapper" style={{marginTop: "140px"}}>
                    <div className="outer-wrapper">
                        <div id="documentation"><Documentation currentlyOpen={this.state.documentationOpen}/></div>
                        <div id="configuration" style={{display: "none"}}><Configuration currentlyOpen={this.state.configurationOpen}/></div>
                        <div id="monitoring-application" style={{display: "none"}}><MonitoringApplication currentlyOpen={this.state.monitorApplicationOpen}/></div>
                        <div id="monitoring-algorithm" style={{display: "none"}}><MonitoringAlgorithm currentlyOpen={this.state.monitorAlgorithmOpen}/></div>
                    </div>
                </div>
                <Layout>
                    <Footer style={{textAlign: "center"}}>
                        <span className="footer">ENACT ©2018-2020</span>
                    </Footer>
                </Layout>
            </Layout>
        );
    }
}

export default App;