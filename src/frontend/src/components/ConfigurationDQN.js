import React from "react";
import {Card, Typography, Input, Button, Popconfirm, message, Upload, Radio} from 'antd';
import {UploadOutlined, DownloadOutlined} from '@ant-design/icons';
import SpaceSpecifierCard from "./SpaceSpecifierCard";
import backendUrl from "./BackendUrl";

const { Paragraph, Text} = Typography;

class ConfigurationDQN extends React.Component {
    hiddenLinkForDownload = React.createRef();

    state = {
        learning_rate: 0.0001,
        buffer_size: 1000000,
        batch_size: 32,
        tau: 1.0,
        gamma: 0.99,
        train_freq: 4,
        gradient_steps: 1,
        target_update_interval: 10000,
        eps: 0.1,
        max_grad_norm: 0.1,
        seed: null,
        currentlyUploading: false,
        currentActionSpaceSpec: null,
        currentStateSpaceSpec: null,
    };

    confirmUpdate = () => {
        if (!('currentStateSpaceSpec' in this.state) || !('currentActionSpaceSpec' in this.state)) {
            message.error('State and action space have to be specified!')
            return
        }

        let data = {
            algorithm: 'dqn',
            state_space_data: this.state.currentStateSpaceSpec,
            action_space_data: this.state.currentActionSpaceSpec,
            learning_rate: this.state.learning_rate,
            buffer_size: this.state.buffer_size,
            batch_size: this.state.batch_size,
            tau: this.state.tau,
            gamma: this.state.gamma,
            train_freq: this.state.train_freq,
            gradient_steps: this.state.gradient_steps,
            target_update_interval: this.state.target_update_interval,
            eps: this.state.eps,  // constant epsilon (continuing environment)
            max_grad_norm: this.state.max_grad_norm,
        }

        if (this.state.seed !== null)
            data.seed = this.state.seed

        fetch(backendUrl + '/api/submit_configuration/',
            {
                method: 'post',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            })
            .then(response => {
                console.log(response);
                return response.json();
            }).then(json => {
                if ('error' in json) {
                    message.error(json['error']);
                } else {
                    message.success('Restarted Learning Process!');
                }
            }).catch(function (error) {
                message.error('An error occurred!')
                console.log(error);
            });
    }
    
    onChange = (e) => {
        this.setState({[e.target.name]: e.target.value});
    }

    getStateSpaceInformation = (data) => {
        this.setState({currentStateSpaceSpec: data});
    }

    getActionSpaceInformation = (data) => {
        this.setState({currentActionSpaceSpec: data});
    }

    onUploadPressed = (file) => {
        this.setState({currentlyUploading: true});

        let context = this;
        let reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => {
            fetch(backendUrl + '/api/upload_model/',
                {
                    method: 'post',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        algorithm: 'dqn',
                        base_64_string: reader.result.split(',').pop()
                    })
                })
                .then(response => {
                    context.setState({currentlyUploading: false});
                    return response.json();
                }).then(json => {
                    if ('error' in json) {
                        message.error(json['error']);
                    } else {
                        message.success('Model loaded successfully!');
                        context.setState({
                            currentActionSpaceSpec: json.action_space_specs,
                            currentStateSpaceSpec: json.state_space_specs,
                            learning_rate: json.learning_rate,
                            buffer_size: json.buffer_size,
                            batch_size: json.batch_size,
                            tau: json.tau,
                            gamma: json.gamma,
                            train_freq: json.train_freq,
                            gradient_steps: json.gradient_steps,
                            target_update_interval: json.target_update_interval,
                            eps: json.exploration_final_eps,
                            max_grad_norm: json.max_grad_norm,
                            seed: json.seed
                        });
                    }
            }).catch(function (error) {
                message.error('An error occurred!')
            });
        };

        reader.onerror = (error) => {
            context.setState({currentlyUploading: false});
            message.error("Error parsing file!")
        };
    };

    onDownloadClicked = () => {
        fetch(backendUrl + '/api/download_model/')
            .then(resp => resp.blob())
            .then(blob => {
                const url = window.URL.createObjectURL(blob);
                const a = this.hiddenLinkForDownload.current;
                a.href = url;
                a.download = 'model.zip';
                a.click();
                a.href = ''
            })
            .catch((error) => {
                console.log(error);
                message.error("An error occurred!")
            });
    }
    
    render() {
        return (
            <>
                <Paragraph>
                    <Card headStyle={{fontWeight: 'bold', fontSize: '200%'}} title={"Load Pretrained Model"}>
                        <div style={{display: 'flex'}}>
                            <Upload beforeUpload={this.onUploadPressed} showUploadList={false} accept={".zip"}>
                                <Button icon={<UploadOutlined/>} loading={this.state.currentlyUploading}>
                                    Upload Pretrained Model
                                </Button>
                            </Upload>
                            <Button style={{marginLeft: '20px'}} onClick={this.onDownloadClicked}>
                                <DownloadOutlined/> Download Current Model
                            </Button>
                            <a style={{display: 'hidden'}} ref={this.hiddenLinkForDownload}/>
                        </div>
                        <ul style={{marginTop: '20px'}}>
                            <li>
                                <Text>Upload a .zip file that was created by <b>stable-baselines</b> with the method <b>DQN.save()</b>.</Text>
                            </li>
                            <li>
                                <Text>Wait until the configuration is updated below.</Text>
                            </li>
                        </ul>
                    </Card>
                </Paragraph>
                <SpaceSpecifierCard currentSpaceSpec={this.state.currentStateSpaceSpec} title='State Space' description='Is the agent supposed to have discrete or continuous values as inputs?' parentCallback={this.getStateSpaceInformation}/>
                <SpaceSpecifierCard disableContinuous={true} currentSpaceSpec={this.state.currentActionSpaceSpec} title='Action Space' description='Is the agent supposed to output discrete or continuous values?' parentCallback={this.getActionSpaceInformation}/>
                <Paragraph>
                    <Card headStyle={{fontWeight: 'bold', fontSize: '200%'}} title={"Hyperparameter"}>
                        <br/><Text><b>Learning Rate</b></Text><br/>
                        <Input value={this.state.learning_rate} onChange={this.onChange} name='learning_rate' style={{marginTop: '10px', marginBottom: '20px', maxWidth: '500px'}}/>
                        <br/><Text><b>Buffer Size</b></Text><br/>
                        <Input value={this.state.buffer_size} onChange={this.onChange} name='buffer_size' style={{marginTop: '10px', marginBottom: '20px', maxWidth: '500px'}}/>
                        <br/><Text><b>Batch Size</b></Text><br/>
                        <Input value={this.state.batch_size} onChange={this.onChange} name='batch_size' style={{marginTop: '10px', marginBottom: '20px', maxWidth: '500px'}}/>
                        <br/><Text><b>Tau</b></Text><br/>
                        <Input value={this.state.tau} onChange={this.onChange} name='tau' style={{marginTop: '10px', marginBottom: '20px', maxWidth: '500px'}}/>
                        <br/><Text><b>Gamma</b></Text><br/>
                        <Input value={this.state.gamma} onChange={this.onChange} name='gamma' style={{marginTop: '10px', marginBottom: '20px', maxWidth: '500px'}}/>
                        <br/><Text><b>Training Frequency</b></Text><br/>
                        <Input value={this.state.train_freq} onChange={this.onChange} name='train_freq' style={{marginTop: '10px', marginBottom: '20px', maxWidth: '500px'}}/>
                        <br/><Text><b>Gradient Steps</b></Text><br/>
                        <Input value={this.state.gradient_steps} onChange={this.onChange} name='gradient_steps' style={{marginTop: '10px', marginBottom: '20px', maxWidth: '500px'}}/>
                        <br/><Text><b>Target Network Update Interval</b></Text><br/>
                        <Input value={this.state.target_update_interval} onChange={this.onChange} name='target_update_interval' style={{marginTop: '10px', marginBottom: '20px', maxWidth: '500px'}}/>
                        <br/><Text><b>Epsilon</b></Text><br/>
                        <Input value={this.state.eps} onChange={this.onChange} name='eps' style={{marginTop: '10px', marginBottom: '20px', maxWidth: '500px'}}/>
                        <br/><Text><b>Maximum Value for the Gradient Clipping</b></Text><br/>
                        <Input value={this.state.max_grad_norm} onChange={this.onChange} name='max_grad_norm' style={{marginTop: '10px', marginBottom: '20px', maxWidth: '500px'}}/>
                        <br/><Text><b>Seed</b></Text><br/>
                        <Input value={this.state.seed} onChange={this.onChange} name='seed' style={{marginTop: '10px', marginBottom: '20px', maxWidth: '500px'}}/>
                    </Card>
                </Paragraph>
                <Popconfirm
                    title="This will reset the learning process. Continue?"
                    onConfirm={this.confirmUpdate}
                    okText="Yes"
                    cancelText="Abort"
                >
                    <Button type="primary">
                        Update Configuration and Restart Learning
                    </Button>
                </Popconfirm>
            </>
        );
    }
}

export default ConfigurationDQN;