import React from "react";
import {Card, Typography, Input, Button, Popconfirm, message, Upload, Radio} from 'antd';
import {UploadOutlined, DownloadOutlined} from '@ant-design/icons';
import SpaceSpecifierCard from "./SpaceSpecifierCard";
import backendUrl from "./BackendUrl";
import ConfigurationPPO from "./ConfigurationPPO";
import ConfigurationDQN from "./ConfigurationDQN";

const { Paragraph, Text} = Typography;

class Configuration extends React.Component {
    state = {
        algorithm: ''
    };

    onChange = (e) => {
        console.log(e.target.name)
        this.setState({[e.target.name]: e.target.value});
    }

    render() {
        return (
            <>
                <Paragraph>
                    <Card headStyle={{fontWeight: 'bold', fontSize: '200%'}} title={"Select Algorithm"}>
                        <Radio.Group name={'algorithm'} onChange={this.onChange} value={this.state.algorithm}>
                            <Radio.Button value={'ppo'}>Proximal Policy Optimization</Radio.Button>
                            <Radio.Button value={'dqn'} style={{marginLeft: '1px'}}>Deep Q-Network</Radio.Button>
                        </Radio.Group>
                    </Card>
                </Paragraph>
                <div style={{display: this.state.algorithm === 'ppo' ? 'block' : 'none'}}>
                    <ConfigurationPPO />
                </div>
                <div style={{display: this.state.algorithm === 'dqn' ? 'block' : 'none'}}>
                    <ConfigurationDQN />
                </div>
            </>
        );
    }
}

export default Configuration;