import json
import os
from stable_baselines3 import PPO
from stable_baselines3 import DQN
import csv

import h5py
import numpy as np
from django.conf import settings
from django.http import HttpResponse, Http404
from django.http import JsonResponse
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.csrf import csrf_exempt
from gym import spaces
from .utils import extract_ppo_config_from_model, write_64_string_to_file, append_rows_to_h5_dataset, \
                   append_row_to_h5_dataset, extract_dqn_config_from_model


class InvalidConfigurationException(Exception):
    pass


def persist_new_configuration_to_file(default_configuration, current_configuration, new_configuration):
    # persist new configuration to file
    configuration_update = {**default_configuration, **current_configuration, **new_configuration}
    with open(settings.CONFIGURATION_FILE_NAME, 'w') as json_file:
        json.dump(configuration_update, json_file)


@method_decorator(csrf_exempt, name='dispatch')
class SubmitObservationAndLastRewardGetAction(View):
    def get(self, request):
        return JsonResponse({'error': 'POST the current state representation and the last reward (if existent) '
                                      'against this endpoint'}, status=400)

    def post(self, request):
        response = json.loads(request.body)
        if 'observation' not in response:
            return JsonResponse({'error': 'POST the current state representation and the last reward (if existent) '
                                          'against this endpoint'}, status=400)

        validated_observation = None
        if type(request.ppo_agent.observation_space) == spaces.Box:
            if not isinstance(response['observation'], list) or len(response['observation']) != request.ppo_agent.observation_space.shape[0]:
                return JsonResponse({'error': "'observation' has to be an array with exactly as many floats as there "
                                              "are state dimensions."}, status=400)

            try:
                validated_observation = np.array([float(dim) for dim in response['observation']])
            except (ValueError, TypeError):
                return JsonResponse({'error': "'observation' has to be an array with exactly as many floats as there "
                                              "are state dimensions."}, status=400)

        elif type(request.ppo_agent.observation_space) == spaces.Discrete:
            validated_observation = response['observation'][0] if isinstance(response['observation'], list) else response['observation']

            try:
                validated_observation = int(validated_observation)
            except (ValueError, TypeError):
                return JsonResponse({'error': "'observation' has to be exactly one integer."}, status=400)

            if validated_observation >= request.ppo_agent.action_space.n:
                return JsonResponse({'error': "'observation' has exceeded maximal allowed number (%i)."
                                              % request.ppo_agent.action_space.n}, status=400)
            # TODO: check if discrete value as state space have to be packed into an array

        try:
            reward = float(response['reward']) if 'reward' in response else None
        except (ValueError, TypeError):
            return JsonResponse({'error': "'reward' has to be a scalar value."}, status=400)

        # calculate action
        action = request.ppo_agent.get_next_action(validated_observation, reward)
        if type(request.ppo_agent.action_space) == spaces.Box:
            action = action.tolist()
        elif type(request.ppo_agent.action_space) == spaces.Discrete:
            action = int(action)

        return JsonResponse({'action': action}, status=200)


@method_decorator(csrf_exempt, name='dispatch')
class SubmitConfiguration(View):
    @staticmethod
    def _validate_algorithm_configuration(configuration, hyperparameter_types):
        validated_configuration = {}

        for param, value_type in hyperparameter_types.items():
            if param not in configuration:
                continue

            if 'optional' in value_type and configuration[param] is None:
                validated_configuration[param] = None
                continue

            if 'float' in value_type:
                try:
                    validated_configuration[param] = float(configuration[param])
                except (ValueError, TypeError):
                    raise InvalidConfigurationException("'%s' has to be a float." % param)
            elif 'int' in value_type:
                try:
                    validated_configuration[param] = int(configuration[param])
                except (ValueError, TypeError):
                    raise InvalidConfigurationException("'%s' has to be an int." % param)
            elif 'bool' in value_type:
                try:
                    validated_configuration[param] = bool(configuration[param])
                except (ValueError, TypeError):
                    raise InvalidConfigurationException("'%s' has to be a boolean." % param)

        return validated_configuration

    @staticmethod
    def _validate_dqn_configuration(configuration):
        hyperparameter_types = {
            'learning_rate': 'float',
            'buffer_size': 'int',
            'batch_size': 'int',
            'tau': 'float',
            'gamma': 'float',
            'train_freq': 'int',
            'gradient_steps': 'int',
            'target_update_interval': 'int',
            'eps': 'float',
            'max_grad_norm': 'float',
        }

        validated_configuration = SubmitConfiguration._validate_algorithm_configuration(configuration, hyperparameter_types)
        eps = validated_configuration.pop('eps', None)
        if eps is not None:  # continuing environment, therefore decaying epsilon makes no sense
            validated_configuration['exploration_fraction'] = eps
            validated_configuration['exploration_final_eps'] = eps
            validated_configuration['exploration_initial_eps'] = eps

        return validated_configuration

    @staticmethod
    def _validate_ppo_configuration(configuration):
        hyperparameter_types = {
            'learning_rate': 'float',
            'n_steps': 'int',
            'batch_size': 'int',
            'n_epochs': 'int',
            'gamma': 'float',
            'gae_lambda': 'float',
            'clip_range': 'float',
            'clip_range_vf': 'float optional',
            'ent_coef': 'float',
            'vf_coef': 'float',
            'max_grad_norm': 'float',
            'use_sde': 'bool',
            'sde_sample_freq': 'int',
            'target_kl': 'float optional',
        }

        return SubmitConfiguration._validate_algorithm_configuration(configuration, hyperparameter_types)

    @staticmethod
    def _validate_configuration(configuration):
        validated_configuration = {}

        if 'action_space_data' not in configuration or configuration['action_space_data'] is None:
            raise InvalidConfigurationException('Action space information not sent.')

        if 'state_space_data' not in configuration or configuration['state_space_data'] is None:
            raise InvalidConfigurationException('State space information not sent.')

        validated_configuration['action_space_specs'] = SubmitConfiguration._validate_space_configuration(configuration['action_space_data'])
        validated_configuration['state_space_specs'] = SubmitConfiguration._validate_space_configuration(configuration['state_space_data'])

        if 'seed' in configuration:
            try:
                validated_configuration['seed'] = int(configuration['seed'])
            except (ValueError, TypeError):
                raise InvalidConfigurationException("'seed' has to be an int.")

        if 'algorithm' in configuration:
            validated_configuration['algorithm'] = configuration['algorithm'].lower()
        else:
            raise InvalidConfigurationException('Algorithm (DQN|PPO) was not specified.')

        if validated_configuration['algorithm'] == 'ppo':
            return {**validated_configuration, **SubmitConfiguration._validate_ppo_configuration(configuration)}
        elif validated_configuration['algorithm'] == 'dqn':
            return {**validated_configuration, **SubmitConfiguration._validate_dqn_configuration(configuration)}
        else:
            raise InvalidConfigurationException('Unsupported algorithm specified (only DQN|PPO).')

    @staticmethod
    def _validate_space_configuration(space_configuration):
        validated_space_configuration = {}
        if 'type' not in space_configuration or space_configuration['type'] not in ['discrete', 'continuous']:
            raise InvalidConfigurationException("Wrong 'type' attribute specified in space definition. Valid types: 'discrete' and 'continuous'.")

        if space_configuration['type'] == 'discrete':
            if 'number_of_discrete_values' not in space_configuration:
                raise InvalidConfigurationException("'number_of_discrete_values' has to be specified, when using discrete space.")

            validated_space_configuration['type'] = 'discrete'
            try:
                validated_space_configuration['number_of_discrete_values'] = int(space_configuration['number_of_discrete_values'])
            except (ValueError, TypeError):
                raise InvalidConfigurationException("'number_of_discrete_values' has must be an integer.")

        if space_configuration['type'] == 'continuous':
            if 'bounds' not in space_configuration or not isinstance(space_configuration['bounds'], list):
                raise InvalidConfigurationException("'bounds' array has to be specified, when using continuous space.")

            verified_bounds = []
            for bound in space_configuration['bounds']:
                if not isinstance(bound, list) or len(bound) != 3:
                    raise InvalidConfigurationException("All items in the 'bounds' array have to be "
                                                                            "arrays with exactly three elements (label, upper and lower bound).")

                try:
                    label = str(bound[0])
                    lower_bound = '-inf' if bound[1] == '-inf' else float(bound[1])
                    upper_bound = 'inf' if bound[2] == 'inf' else float(bound[2])
                except (ValueError, TypeError):
                    raise InvalidConfigurationException("All bounds have to be floats or strings ('-inf' for unbounded lower bound and 'inf' for unbounded upper bound).")

                verified_bounds.append([label, lower_bound, upper_bound])

            validated_space_configuration['type'] = 'continuous'
            validated_space_configuration['bounds'] = verified_bounds

        return validated_space_configuration

    def get(self, request):
        return JsonResponse({'error': 'POST the configuration against this endpoint'}, status=400)

    def post(self, request):
        # validate request
        request_content = json.loads(request.body)

        try:
            validated_configuration = self._validate_configuration(request_content)
        except InvalidConfigurationException as e:
            return JsonResponse({'error': str(e)}, status=400)

        current_configuration = {}
        if os.path.exists(settings.CONFIGURATION_FILE_NAME):
            with open(settings.CONFIGURATION_FILE_NAME) as json_file:
                stored_configuration = json.load(json_file)

            if stored_configuration['algorithm'] == validated_configuration['algorithm']:
                current_configuration = stored_configuration

        default_configuration = settings.DEFAULT_PPO_CONFIG if validated_configuration['algorithm'] == 'ppo' else settings.DEFAULT_DQN_CONFIG
        configuration_update = {}
        for configuration in default_configuration:
            if configuration in validated_configuration:
                if configuration not in current_configuration or \
                   current_configuration[configuration] != validated_configuration[configuration]:
                    configuration_update[configuration] = validated_configuration[configuration]

        if 'state_space_specs' not in current_configuration or str(current_configuration['state_space_specs']) != str(validated_configuration['state_space_specs']):
            configuration_update['state_space_specs'] = validated_configuration['state_space_specs']

        if 'action_space_specs' not in current_configuration or str(current_configuration['action_space_specs']) != str(validated_configuration['action_space_specs']):
            configuration_update['action_space_specs'] = validated_configuration['action_space_specs']

        if not configuration_update:
            return JsonResponse({'error': 'No new configurations were sent'}, status=400)

        persist_new_configuration_to_file(default_configuration, current_configuration, configuration_update)

        # restart learn process
        request.restart_learn_process = True

        return JsonResponse({'success': 'The learn process has restarted with the new configuration'}, status=200)


@method_decorator(csrf_exempt, name='dispatch')
class GetEnvironmentalLogData(View):
    def get(self, request):
        try:
            first_timestep = int(request.GET['first_timestep']) if 'first_timestep' in request.GET else 0
            first_timestep = max(first_timestep, 0)  # no negative values allowed
        except (ValueError, TypeError):
            return JsonResponse({'error': "'first_timestep' parameter has to be of type integer"}, status=400)

        with h5py.File(settings.LOG_DATABASE_NAME, 'r', libver='latest', swmr=True) as log_file:
            observation_labels = log_file['observations'].attrs['labels']
            action_labels = log_file['actions'].attrs['labels']

            observation_logs = log_file['observations'][first_timestep:first_timestep + settings.MAX_NUMBER_OF_LOGS_PER_REQUEST]
            action_logs = log_file['actions'][first_timestep:first_timestep + settings.MAX_NUMBER_OF_LOGS_PER_REQUEST]
            reward_logs = log_file['rewards'][first_timestep:first_timestep + settings.MAX_NUMBER_OF_LOGS_PER_REQUEST]

        # ensure all logs have the same number of entries
        lowest_number_of_entries = min(observation_logs.shape[0], action_logs.shape[0], reward_logs.shape[0])

        return JsonResponse({
            'starting_from_timestep': first_timestep,
            'observation_labels': observation_labels.tolist(),
            'observations': observation_logs[:lowest_number_of_entries].tolist(),
            'action_labels': action_labels.tolist(),
            'actions': action_logs[:lowest_number_of_entries].tolist(),
            'rewards': [reward[0] for reward in reward_logs.tolist()][:lowest_number_of_entries],
            'algorithm_data_labels': [],  # TODO: use this for std, mean, hyperparams ...
            'algorithm_data': [],  # TODO: use this for std, mean, hyperparams ...
            'other_data_labels': [],  # TODO: use this for baseline data, benchmarks, BDA value ... REMOVE
            'other_data': [],  # TODO: use this for baseline data, benchmarks, BDA value ... REMOVE
        }, status=200)

    def post(self, request):
        return JsonResponse({'error': 'GET request allowed only'}, status=400)


class ValidationError(Exception):
    pass


@method_decorator(csrf_exempt, name='dispatch')
class GetAvailableExternalLogDataIds(View):
    def get(self, request):
        with h5py.File(settings.EXTERNAL_DATA_DATABASE, 'r', libver='latest', swmr=True) as log_file:
            return JsonResponse({'external_data_ids': list(log_file)}, status=200)

    def post(self, request):
        JsonResponse({'error': 'GET request allowed only'}, status=400)


@method_decorator(csrf_exempt, name='dispatch')
class DiagramAnnotation(View):
    def get(self, request):
        with open('annotations.csv', 'r', newline='') as annotations_file:
            raw_annotations = list(csv.reader(annotations_file))
            annotations = [{'timestep': int(annotation[0]),
                            'text': annotation[1]} for annotation in raw_annotations]

            return JsonResponse({'annotations': annotations}, status=200)

    def _validate_post_request(self, request):
        try:
            post_data = json.loads(request.body)
            timestep = int(post_data['timestep'])
            text = post_data['text']
        except (KeyError, ValueError):
            raise ValidationError("JSON body containing 'timestep' and 'text' elements is mandatory")

        return timestep, text

    def post(self, request):
        try:
            timestep, text = self._validate_post_request(request)
        except ValidationError as e:
            return JsonResponse({'error': str(e)}, status=400)

        with open('annotations.csv', 'a', newline='') as annotations_file:
            csv.writer(annotations_file).writerow((timestep, text))

        return JsonResponse({'success': 'data submitted'}, status=200)


@method_decorator(csrf_exempt, name='dispatch')
class ExternalLogData(View):
    def get(self, request):
        try:
            first_timestep = int(request.GET['first_timestep']) if 'first_timestep' in request.GET else 0
            first_timestep = max(first_timestep, 0)  # no negative values allowed
        except (ValueError, TypeError):
            return JsonResponse({'error': "'first_timestep' parameter has to be of type integer"}, status=400)

        try:
            external_data_id = str(request.GET['unique_title'])
        except KeyError:
            return JsonResponse({'error': "'unique_title' parameter is mandatory"}, status=400)

        with h5py.File(settings.EXTERNAL_DATA_DATABASE, 'r', libver='latest', swmr=True) as log_file:
            if external_data_id not in log_file:
                return JsonResponse({'error': "'%s' is not a valid set of external data" % external_data_id}, status=400)

            external_data_labels = log_file[external_data_id].attrs['labels']
            external_data = log_file[external_data_id][first_timestep:first_timestep + settings.MAX_NUMBER_OF_LOGS_PER_REQUEST]

            return JsonResponse({
                'unique_title': external_data_id,
                'starting_from_timestep': first_timestep,
                'labels': external_data_labels.tolist(),
                'data': external_data.tolist()
            }, status=200)

    def _validate_post_request(self, request):
        try:
            post_data = json.loads(request.body)
            external_data_id = str(post_data['unique_title'])
            labels = post_data['labels']
            data = post_data['data']
        except KeyError:
            raise ValidationError("JSON body containing 'unique_title', 'labels' and 'data' elements is mandatory")

        if not isinstance(labels, list) or not isinstance(data, list):
            raise ValidationError("'labels' and 'data' must be arrays")

        if not labels or not data:
            raise ValidationError("'labels' and 'data' must not be empty")

        if not isinstance(data[0], list):
            raise ValidationError("'data' must be an array of arrays")

        if not len(data[0]) == len(labels):
            raise ValidationError("each element of 'data' must be an array with as many elements as there are labels")

        return external_data_id, labels, data

    def post(self, request):
        try:
            external_data_id, labels, data = self._validate_post_request(request)
        except ValidationError as e:
            return JsonResponse({'error': str(e)}, status=400)

        with h5py.File(settings.EXTERNAL_DATA_DATABASE, 'r+', libver='latest', swmr=True) as log_file:
            if external_data_id not in log_file:
                dataset = log_file.create_dataset(
                    external_data_id,
                    shape=(0, len(labels)),
                    maxshape=(None, len(labels)),
                    chunks=True
                )

                dataset.attrs['labels'] = labels

            append_rows_to_h5_dataset(log_file[external_data_id], data)

        return JsonResponse({'success': 'data submitted'}, status=200)


@method_decorator(csrf_exempt, name='dispatch')
class UploadModel(View):
    def get(self, request):
        JsonResponse({'error': 'POST request allowed only'}, status=400)

    def post(self, request):
        try:
            algorithm_type = json.loads(request.body)['algorithm'].lower()
            assert algorithm_type in ('dqn', 'ppo')
        except (KeyError, AssertionError, Exception):
            return JsonResponse({'error': "Send 'algorithm' as parameter. "
                                          "The parameter must have the value 'dqn' or 'ppo'."}, status=400)

        try:
            base_64_string = json.loads(request.body)['base_64_string']
        except KeyError:
            return JsonResponse({'error': "Send 'base_64_string' as parameter."}, status=400)

        file_path = os.path.join(settings.BASE_DIR, 'stored_model.zip')
        write_64_string_to_file(base_64_string, file_path)

        try:
            if algorithm_type == 'dqn':
                verified_model = DQN.load(file_path)
                new_model_config = extract_dqn_config_from_model(verified_model)
                persist_new_configuration_to_file(settings.DEFAULT_DQN_CONFIG, {}, new_model_config)
            else:
                verified_model = PPO.load(file_path)
                new_model_config = extract_ppo_config_from_model(verified_model)
                persist_new_configuration_to_file(settings.DEFAULT_PPO_CONFIG, {}, new_model_config)
        except:
            os.remove(file_path)
            return JsonResponse({'error': 'Invalid file sent. Send a .zip generated by PPO (stable-baselines 3).'}, status=400)

        # restart learn process
        request.restart_learn_process = True

        return JsonResponse(new_model_config, status=200)


def download_model(request):
    file_path = os.path.join(settings.BASE_DIR, 'stored_model.zip')
    if os.path.exists(file_path):
        with open(file_path, 'rb') as fh:
            response = HttpResponse(fh.read(), content_type="application/zip")
            response['Content-Disposition'] = 'inline; filename=ppo_model.zip'
            return response

    else:
        raise Http404('No stored model available.')


# serve the frontend
def index(request):
    return render(request, "build/index.html")
