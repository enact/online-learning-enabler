from queue import Queue
from threading import Thread
from stable_baselines3 import DQN
from stable_baselines3.dqn import MlpPolicy
import gym
import numpy as np
from gym import spaces
import os
from gym.spaces import Discrete, Box
from typing import Union
import h5py
from .utils import append_row_to_h5_dataset, extract_dqn_config_from_model


class DQNAsyncAgent:
    class SocketEnvWrapper(gym.Env):
        """Custom Environment that receives and sends data via sockets and thus can be run in a thread."""

        def __init__(self, action_space, observation_space):
            super(DQNAsyncAgent.SocketEnvWrapper, self).__init__()
            self.action_space = action_space
            self.observation_space = observation_space
            self.in_queue = Queue()
            self.out_queue = Queue()
            self.sentinel = object()

        def _pop_item_from_queue(self):
            data = self.in_queue.get()
            if data is self.sentinel:
                raise InterruptedError()

            return data

        def step(self, action):
            self.out_queue.put(action)
            observation, reward = self._pop_item_from_queue()
            return observation, reward, False, {}

        def reset(self):
            observation, _ = self._pop_item_from_queue()
            return observation

        def render(self, mode='human'):
            pass

    def __init__(self, action_space: Discrete, observation_space: Union[Discrete, Box], config=None,
                 stored_model_name='stored_model', log_database_file_name='logs.h5'):
        self.action_space = action_space
        assert type(action_space) == Discrete, 'Only discrete action spaces are allowed for DQN.'
        self.observation_space = observation_space
        self.env = self.SocketEnvWrapper(self.action_space, self.observation_space)
        self.config = config if config is not None else dict()
        self.stored_model_name = stored_model_name

        if os.path.exists(log_database_file_name):
            os.remove(log_database_file_name)

        self.__log_database = h5py.File(log_database_file_name, 'w', libver='latest')
        number_of_observations_dimensions = observation_space.shape[0] if type(observation_space) == Box else 1
        self.__observation_logs = self.__log_database.create_dataset(
            'observations',
            shape=(0, number_of_observations_dimensions),
            maxshape=(None, number_of_observations_dimensions),
            chunks=True
        )
        self.__observation_logs.attrs['labels'] = observation_space.labels if type(observation_space) == Box else ['State']

        number_of_action_dimensions = action_space.shape[0] if type(action_space) == Box else 1
        self.__action_logs = self.__log_database.create_dataset(
            'actions',
            shape=(0, number_of_action_dimensions),
            maxshape=(None, number_of_action_dimensions),
            chunks=True
        )
        self.__action_logs.attrs['labels'] = action_space.labels if type(action_space) == Box else ['Action']

        self.__reward_logs = self.__log_database.create_dataset(
            'rewards',
            shape=(0, 1),
            maxshape=(None, 1),
            chunks=True
        )

        self.__log_database.swmr_mode = True

        self.__agent = Thread(target=self.__agent_thread)
        self.__agent.daemon = True
        self.__agent.start()

    def get_next_action(self, observation, last_reward=None):
        self.env.in_queue.put((observation, last_reward if last_reward is not None else 0.0))
        action = self.env.out_queue.get()

        # log observation, reward and action TODO: log algorithm internals
        append_row_to_h5_dataset(self.__observation_logs, observation)
        append_row_to_h5_dataset(self.__action_logs, action)
        append_row_to_h5_dataset(self.__reward_logs, last_reward if last_reward is not None else 0.0)

        return action

    def store_model_and_shutdown(self):
        self.env.in_queue.put(self.env.sentinel)

    def __agent_thread(self):
        model = None
        if os.path.exists(self.stored_model_name + '.zip'):
            try:
                model = DQN.load(self.stored_model_name)

                # validate configuration
                model_config = extract_dqn_config_from_model(model)
                del model_config['action_space_specs']  # action & state space are validated in set_env below
                del model_config['state_space_specs']
                assert all([model_config[key] == self.config[key]] for key in self.config.keys())

                model.set_env(self.env)
                print('Restored model from %s' % self.stored_model_name)
            except:
                print('Could not recycle model')
                model = None

        if model is None:
            if 'exploration_initial_eps' not in self.config or 'exploration_final_eps' not in self.config:
                self.config['exploration_initial_eps'] = 0.15
                self.config['exploration_final_eps'] = 0.15

            if self.config['exploration_initial_eps'] != self.config['exploration_final_eps']:
                self.config['exploration_initial_eps'] = self.config['exploration_final_eps']

            self.config['learning_starts'] = 10

            model = DQN(MlpPolicy, self.env, **self.config)
            model.save(self.stored_model_name)

        try:
            # model.learn(total_timesteps=np.iinfo(np.int64).max)
            while True:
                model.learn(total_timesteps=10000)
                self.env.out_queue.put(self.env.action_space.sample())
                print('Storing model!')
                model.save(self.stored_model_name)
        except Exception as e:
            print(str(e))
            print('Storing model!')
            model.save(self.stored_model_name)  # to load: model = PPO2.load("stored_model")


if __name__ == '__main__':  # Only for testing and validation purposes!
    state_space = spaces.Box(np.array([np.finfo(np.float32).max] * 4), np.array([np.finfo(np.float32).min] * 4), dtype=np.float64)
    state_space.labels = ['1', '2', '3', '4']

    agent = DQNAsyncAgent(
        spaces.Discrete(2),
        state_space
    )

    environment = gym.make('CartPole-v1')
    observation = environment.reset()

    returns = []  # contains cumulative rewards of past episodes
    rewards = []  # contains rewards of current episode

    reward = 0.0
    try:
        while True:
            if len(returns) == 20:  # print average of last twenty returns
                print(np.mean(returns))
                returns = []

            action = agent.get_next_action(observation, reward)
            observation, reward, done, info = environment.step(action)
            rewards.append(reward)

            if done:  # we need a continuing environment, so episodic is transformed
                returns.append(np.sum(rewards))
                rewards = []
                observation = environment.reset()
                reward = -50  # failure is punished here

    except:
        agent.store_model_and_shutdown()
