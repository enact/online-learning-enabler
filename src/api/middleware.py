import copy
import json
import os
import warnings

import numpy as np
from django.conf import settings
from gym import spaces
from .utils import extract_ppo_config_from_model, convert_space_to_space_spec, convert_space_spec_to_space

from .ppo2_async_agent import PPOAsyncAgent
from .dqn_async_agent import DQNAsyncAgent


class RLMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response
        self.__agent = None
        self.current_config = settings.DEFAULT_PPO_CONFIG

        self.start_learn_process()

    def __call__(self, request):
        # inject rl agent
        request.ppo_agent = self.__agent
        request.restart_learn_process = False
        response = self.get_response(request)

        if request.restart_learn_process:
            self.start_learn_process()

        return response

    def start_learn_process(self):
        if not os.path.exists(settings.CONFIGURATION_FILE_NAME):
            with open(settings.CONFIGURATION_FILE_NAME, 'w') as json_file:
                json.dump(settings.DEFAULT_PPO_CONFIG, json_file)
        else:
            with open(settings.CONFIGURATION_FILE_NAME) as json_file:
                self.current_config = json.load(json_file)

        configuration_to_use = copy.deepcopy(self.current_config)  # Leave self.current_config untouched
        action_space_spec = configuration_to_use.pop('action_space_specs')
        state_space_spec = configuration_to_use.pop('state_space_specs')
        algorithm = configuration_to_use.pop('algorithm')

        if algorithm == 'ppo':
            print('Loading PPO agent.')
            self.__agent = PPOAsyncAgent(
                convert_space_spec_to_space(action_space_spec),
                convert_space_spec_to_space(state_space_spec),
                config=configuration_to_use,
                log_database_file_name=settings.LOG_DATABASE_NAME
            )
        elif algorithm == 'dqn':
            print('Loading DQN agent.')
            self.__agent = DQNAsyncAgent(
                convert_space_spec_to_space(action_space_spec),
                convert_space_spec_to_space(state_space_spec),
                config=configuration_to_use,
                log_database_file_name=settings.LOG_DATABASE_NAME
            )
