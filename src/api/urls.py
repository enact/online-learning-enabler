from django.urls import path

from api.views import SubmitConfiguration, SubmitObservationAndLastRewardGetAction, GetEnvironmentalLogData, index,\
                      download_model, UploadModel, ExternalLogData, GetAvailableExternalLogDataIds, DiagramAnnotation

urlpatterns = [
    path('api/submit_configuration/', SubmitConfiguration.as_view(), name='submit_configuration'),
    path('api/get_action/', SubmitObservationAndLastRewardGetAction.as_view(), name='get_action'),
    path('api/get_environmental_log_data/', GetEnvironmentalLogData.as_view(), name='get_environmental_log_data'),
    path('api/get_available_external_log_data_ids/', GetAvailableExternalLogDataIds.as_view(),
         name='get_available_external_log_data_ids'),
    path('api/external_data/', ExternalLogData.as_view(), name='external_data'),
    path('api/annotations/', DiagramAnnotation.as_view(), name='annotations'),
    path('api/download_model/', download_model, name='download_model'),
    path('api/upload_model/', UploadModel.as_view(), name='upload_model'),
    path('', index, name='index')
]
