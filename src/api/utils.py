from gym import spaces
import numpy as np
import base64


def write_64_string_to_file(base_64_string, file_path):
    file_path += '.zip' if not file_path.endswith('.zip') else ''
    base64_bytes = base_64_string.encode('utf-8')
    decoded_bytes = base64.decodebytes(base64_bytes)
    with open(file_path, 'wb') as file_handle:
        file_handle.write(decoded_bytes)


def convert_space_spec_to_space(space_specs):
    if space_specs['type'] == 'discrete':
        return spaces.Discrete(space_specs['number_of_discrete_values'])
    if space_specs['type'] == 'continuous':
        labels = [bound[0] for bound in space_specs['bounds']]
        lower_bounds = [bound[1] if bound[1] != '-inf' else np.finfo(np.float32).min for bound in space_specs['bounds']]
        upper_bounds = [bound[2] if bound[2] != 'inf' else np.finfo(np.float32).max for bound in space_specs['bounds']]
        space = spaces.Box(np.array(lower_bounds), np.array(upper_bounds), dtype=np.float64)
        space.labels = labels  # Do not try this at home ;)
        return space


def is_unbounded_above(bound):
    return bound == np.finfo(bound).max


def is_unbounded_below(bound):
    return bound == np.finfo(bound).min


def convert_space_to_space_spec(space):
    if isinstance(space, spaces.Box):
        space_type = 'continuous'
        dimensions = []
        for dimension_id in range(space.shape[0]):
            low = float(space.low[dimension_id]) if not is_unbounded_below(space.low[dimension_id]) else '-inf'
            high = float(space.high[dimension_id]) if not is_unbounded_above(space.high[dimension_id]) else 'inf'

            if hasattr(space, 'labels'):
                dimensions.append([space.labels[dimension_id], low, high])
            else:
                dimensions.append(['Dimension %i' % dimension_id, low, high])

        return {'type': space_type, 'bounds': dimensions}

    elif isinstance(space, spaces.Discrete):
        return {
            'type': 'discrete',
            'number_of_discrete_values': space.n
        }

    else:
        raise RuntimeError('Only discrete and box spaces are supported!')


def extract_ppo_config_from_model(model):
    layers = model.policy_kwargs['layers'] if hasattr(model, 'policy_kwargs') and 'layers' in model.policy_kwargs else [
        64, 64]
    return {
        'action_space_specs': convert_space_to_space_spec(model.action_space),
        'state_space_specs': convert_space_to_space_spec(model.observation_space),
        'gamma': model.gamma,
        'n_steps': model.n_steps,
        'ent_coef': model.ent_coef,
        'learning_rate': model.learning_rate if type(model.learning_rate) is float or model.learning_rate is None else model.learning_rate(None),
        'vf_coef': model.vf_coef,
        'max_grad_norm': model.max_grad_norm,
        'gae_lambda': model.gae_lambda,
        'batch_size': model.batch_size,
        'n_epochs': model.n_epochs,
        'clip_range': model.clip_range if type(model.clip_range) is float or model.clip_range is None else model.clip_range(None),
        'clip_range_vf': model.clip_range_vf if type(model.clip_range_vf) is float or model.clip_range_vf is None else model.clip_range_vf(None),
        'use_sde': model.use_sde,
        'sde_sample_freq': model.sde_sample_freq,
        'target_kl': model.target_kl,
        'seed': model.seed,
    }


def extract_dqn_config_from_model(model):
    layers = model.policy_kwargs['layers'] if hasattr(model, 'policy_kwargs') and 'layers' in model.policy_kwargs else [
        64, 64]
    return {
        'action_space_specs': convert_space_to_space_spec(model.action_space),
        'state_space_specs': convert_space_to_space_spec(model.observation_space),
        'learning_rate': model.learning_rate,
        'buffer_size': model.buffer_size,
        'learning_starts': model.learning_starts,
        'batch_size': model.batch_size,
        'tau': model.tau,
        'gamma': model.gamma,
        'train_freq': model.train_freq,
        'gradient_steps': model.gradient_steps,
        'n_episodes_rollout': model.n_episodes_rollout,
        'optimize_memory_usage': model.optimize_memory_usage,
        'target_update_interval': model.target_update_interval,
        'exploration_fraction': model.exploration_fraction,
        'exploration_initial_eps': model.exploration_initial_eps,
        'exploration_final_eps': model.exploration_final_eps,
        'max_grad_norm': model.max_grad_norm,
        'seed': model.seed,
    }


def append_row_to_h5_dataset(dataset, row):
    append_rows_to_h5_dataset(dataset, [row])


def append_rows_to_h5_dataset(dataset, rows):
    if type(rows) != np.ndarray:
        if type(rows) not in [list, tuple]:
            rows = [rows]

        rows = np.array(rows)

    dataset.resize(dataset.shape[0] + len(rows), axis=0)
    dataset[-len(rows):] = rows
    dataset.flush()
