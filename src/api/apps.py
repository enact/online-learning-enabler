from django.apps import AppConfig
import h5py
from django.conf import settings

class ApiConfig(AppConfig):
    name = 'api'

    def ready(self):
        # executed on startup
        h5py.File(settings.EXTERNAL_DATA_DATABASE, 'w', libver='latest').close()
        open('annotations.csv', 'w', newline='').close()
