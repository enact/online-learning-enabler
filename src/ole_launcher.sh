#!/bin/bash
service mosquitto start
cd /ole
nohup python manage.py runserver 0.0.0.0:8000 &
sleep 1m
cd /ole/external_data_feeds
nohup python bda_mqtt.py &
tail -f /dev/null