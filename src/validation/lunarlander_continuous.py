import gym
import requests
import numpy as np
import json

BACKEND_BASE_URL = 'http://127.0.0.1:8000/api/'
BACKEND_GET_ACTION_ENDPOINT = 'get_action/'

if __name__ == '__main__':  # Only for testing and validation purposes!
    environment = gym.make('LunarLanderContinuous-v2')
    observation = environment.reset()

    returns = []  # contains cumulative rewards of past episodes
    rewards = []  # contains rewards of current episode

    reward = 0.0

    while True:
        if len(returns) == 20:  # print average of last twenty returns
            print(np.mean(returns))
            returns = []

        # environment.render()
        response = requests.post(BACKEND_BASE_URL + BACKEND_GET_ACTION_ENDPOINT, json={
            'observation': observation.tolist(),
            'reward': reward
        })

        action = json.loads(response.content)['action']
        observation, reward, done, info = environment.step(action)
        rewards.append(reward)

        if done:  # we need a continuing environment, so episodic is transformed
            returns.append(np.sum(rewards))
            rewards = []
            observation = environment.reset()
