import h5py
import numpy as np

def append_row_to_h5_dataset(dataset, row):
    append_rows_to_h5_dataset(dataset, [row])


def append_rows_to_h5_dataset(dataset, rows):
    if type(rows) != np.ndarray:
        if type(rows) not in [list, tuple]:
            rows = [rows]

        rows = np.array(rows)

    dataset.resize(dataset.shape[0] + len(rows), axis=0)
    dataset[-len(rows):] = rows
    dataset.flush()


unique_title = 'neues diagramm'
labels = ['a', 'b', 'c']
data = [[1, 2, 3], [4, 5, 6]]

f = h5py.File('test.h5', 'w', libver='latest')
ds = f.create_dataset('abc', shape=(0, len(labels)), maxshape=(None, len(labels)), chunks=True)
append_rows_to_h5_dataset(ds, data)
# ds = f.create_dataset('abcd', shape=(0, len(labels)), maxshape=(None, len(labels)), chunks=True)
# ds = f.create_dataset('abcde', shape=(0, len(labels)), maxshape=(None, len(labels)), chunks=True)
# ds.attrs['labels'] = labels
pass