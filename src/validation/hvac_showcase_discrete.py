import json

import gym
import requests
import time

BACKEND_BASE_URL = 'http://127.0.0.1:8000/api/'
BACKEND_GET_ACTION_ENDPOINT = 'get_action/'

CHANGE_DYNAMIC_TIMESTEP = 0
DYNAMIC_CHANGE_FACTOR = 5

# config (paste into configuration.json):
# DQN: {"algorithm": "dqn", "learning_rate": 0.0001, "buffer_size": 1000000, "learning_starts": 10, "batch_size": 32, "tau": 1.0, "gamma": 0.99, "train_freq": 4, "gradient_steps": 1, "target_update_interval": 10000, "exploration_fraction": 0.1, "exploration_initial_eps": 0.1, "exploration_final_eps": 0.1, "seed": null, "state_space_specs": {"type": "continuous", "bounds": [["Dimension 0", "-inf", "inf"], ["Dimension 1", "-inf", "inf"], ["Dimension 2", "-inf", "inf"], ["Dimension 3", "-inf", "inf"], ["Dimension 4", "-inf", "inf"]]}, "action_space_specs": {"type": "discrete", "number_of_discrete_values": 7}}

def submit_annotation(timestep, title):
    url = BACKEND_BASE_URL + 'annotations/'
    requests.post(url, json={
        'timestep': timestep,
        'text': title
    })


if __name__ == '__main__':  # Only for testing and validation purposes!

    environment = gym.make(
        'gym_hvac:HVAC-v0',
        total_steps=150_000,
        continuous_action_space=False,
        seed=1,
        observations_space_combination=[1, 1, 1, 1, 0, 0, 0, 0, 1],
        reward_function='clipped'
    )

    observation = environment.reset()
    # change dynamic:
    action = 0

    rewards = []  # contains rewards of current episode

    reward = 0.0
    current_timestep = 1
    while True:
        # environment.render()
        response = requests.post(BACKEND_BASE_URL + BACKEND_GET_ACTION_ENDPOINT, json={
            'observation': observation.tolist(),
            'reward': reward
        })

        action = json.loads(response.content)['action']
        observation, reward, done, info = environment.step(action)
        rewards.append(reward)

        current_timestep += 1

        if current_timestep == CHANGE_DYNAMIC_TIMESTEP:
            environment.HEATING_TEMPERATURE *= DYNAMIC_CHANGE_FACTOR
            submit_annotation(current_timestep, 'Changed Heating Device')
