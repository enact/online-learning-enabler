import tensorflow as tf

summary_writer = None


def log_with_tensorboard(tag, simple_value, step):
    global summary_writer
    if summary_writer is not None:
        summary = tf.Summary(
            value=[tf.Summary.Value(tag=tag, simple_value=simple_value)])
        summary_writer.add_summary(summary, step)


def tensorboard_callback(locals_, globals_):
    global summary_writer
    self_ = locals_['self']
    if summary_writer is None:
        summary_writer = locals_['writer']

    return True
