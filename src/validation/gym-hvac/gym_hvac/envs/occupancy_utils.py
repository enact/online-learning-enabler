import random
import time
import statistics
import numpy as np
from scipy.stats import norm


class OccupancyPatternGenerator:

    def generate_occupancy_patterns(self, total_steps):
        print("generating occupancy pattern...")
        start = time.time()

        temp_pattern = []
        temp_prediction = []

        for time_step in range(total_steps + 1):
            temp_pattern.append(self._calculate_occupancy(time_step))
            temp_prediction.append(self._occupancy_prediction(time_step))

        print("finished occupancy pattern, time: " + str(time.time() - start))
        return temp_pattern, temp_prediction

    def __init__(self, occupancy_pattern, horizon, use_probabilistic_occupancy):
        self.OCCUPANCY_PATTERN = occupancy_pattern
        self.HORIZON = horizon
        self.PROBABILISTIC_OCCUPANCY = use_probabilistic_occupancy

        self.current_occupancy = None
        self.occupancy_change_lock = -1
        self.next_occupancy_change = None

    def _occupancy_prediction(self, timestep):
        return statistics.mean(self._occupancies_of_next_n_timesteps(timestep, probabilistic=False))

    def _occupancies_of_next_n_timesteps(self, timestep, probabilistic):
        occupancies = []
        for step in range(timestep, timestep + self.HORIZON + 1):
            occupancies.append(self._calculate_occupancy(step, probabilistic))

        return occupancies

    def _occupancies_of_last_n_timesteps(self, timestep, probabilistic):
        occupancies = []
        for step in range(timestep, timestep - self.HORIZON - 1, -1):
            occupancies.append(self._calculate_occupancy(step, probabilistic))

        return occupancies

    def _difference_to_occupancy_change(self, occupancies):
        last_occupancy = occupancies[0]
        for i in range(1, len(occupancies)):
            if last_occupancy != occupancies[i]:
                return i
            last_occupancy = occupancies[i]

        return -1

    def _calculate_probabilistic_occupancy(self, timestep):

        if timestep == 0:
            self.current_occupancy = self._calculate_occupancy(timestep, probabilistic=False)

        next_occupancies = self._occupancies_of_next_n_timesteps(timestep, probabilistic=False)
        diff_to_next_real_change = self._difference_to_occupancy_change(next_occupancies)

        if self.occupancy_change_lock < 0 and diff_to_next_real_change == self.HORIZON:
            self.next_occupancy_change = timestep + int(np.random.normal(self.HORIZON, self.HORIZON / 4, 1))
        else:
            self.occupancy_change_lock -= 1

        if timestep == self.next_occupancy_change:
            if self.current_occupancy == 1:
                self.current_occupancy = 0
            else:
                self.current_occupancy = 1
            self.occupancy_change_lock = 2 * self.HORIZON + 1

        return self.current_occupancy

    def _calculate_occupancy(self, timestep, probabilistic=None):

        if probabilistic is None:
            probabilistic = self.PROBABILISTIC_OCCUPANCY

        if probabilistic:
            return self._calculate_probabilistic_occupancy(timestep)

        timestep = timestep % (7 * 24 * 60)  # pattern for one week
        if self.OCCUPANCY_PATTERN == 0:
            return True

        if self.OCCUPANCY_PATTERN == 1 or self.OCCUPANCY_PATTERN == 2:
            # Pattern 1: Weekend from friday 8pm to monday 8am; working from 8-20h / Pattern 2: Opposite of pattern 1
            if timestep < 480 or timestep > 6960:
                # occupancy="weekend"
                occupancy = 1
            elif timestep >= 6240:
                # occupancy="work friday"
                occupancy = 0
            elif timestep >= 5520:
                # occupancy="thursday night"
                occupancy = 1
            elif timestep >= 4800:
                # occupancy="work thursday"
                occupancy = 0
            elif timestep >= 4080:
                # occupancy="wednesday night"
                occupancy = 1
            elif timestep >= 3360:
                # occupancy="work wednesday"
                occupancy = 0
            elif timestep >= 2640:
                # occupancy="tuesday night"
                occupancy = 1
            elif timestep >= 1920:
                # occupancy="work tuesday"
                occupancy = 0
            elif timestep >= 1200:
                # occupancy="monday night"
                occupancy = 1
            elif timestep >= 480:
                # occupancy="work monday"
                occupancy = 0
            else:
                # occupancy="error"
                occupancy = -1

            if self.OCCUPANCY_PATTERN == 1:
                return occupancy
            elif self.OCCUPANCY_PATTERN == 2:
                return not occupancy

        elif self.OCCUPANCY_PATTERN == 3 or self.OCCUPANCY_PATTERN == 4:
            # Pattern 3: Weekend from friday 4pm to monday 9am; working mo-th 9-5pm & fr 8-4pm / Pattern 4: Opposite of
            # pattern 3
            if timestep < 540 or timestep > 6720:
                # occupancy="weekend"
                occupancy = 1
            elif timestep >= 6240:
                # occupancy="work friday"
                occupancy = 0
            elif timestep >= 5340:
                # occupancy="thursday night"
                occupancy = 1
            elif timestep >= 4860:
                # occupancy="work thursday"
                occupancy = 0
            elif timestep >= 3900:
                # occupancy="wednesday night"
                occupancy = 1
            elif timestep >= 3420:
                # occupancy="work wednesday"
                occupancy = 0
            elif timestep >= 2460:
                # occupancy="tuesday night"
                occupancy = 1
            elif timestep >= 1980:
                # occupancy="work tuesday"
                occupancy = 0
            elif timestep >= 1020:
                occupancy = 1
            elif timestep >= 540:
                # occupancy="work monday"
                occupancy = 0
            else:
                # occupancy="error"
                occupancy = -1
            if self.OCCUPANCY_PATTERN == 3:
                return occupancy
            elif self.OCCUPANCY_PATTERN == 4:
                return not occupancy


if __name__ == '__main__':
    gen = OccupancyPatternGenerator(occupancy_pattern=1, horizon=30, use_probabilistic_occupancy=True)
    tmp1, tmp2 = gen.generate_occupancy_patterns(1000)

    print(tmp2)
    import matplotlib.pyplot as plt
    from pylab import *



    plt.plot(tmp2)
    plt.title('Second chart')
    plt.show()

