import json

import gym
import requests

BACKEND_BASE_URL = 'http://127.0.0.1:8000/api/'
BACKEND_GET_ACTION_ENDPOINT = 'get_action/'

CHANGE_DYNAMIC_TIMESTEP = 35_000
DYNAMIC_CHANGE_FACTOR = 5

# config (paste into configuration.json):
# {"algorithm": "ppo", "action_space_specs": {"type": "continuous", "bounds": [["Heating/Cooling", -1.0, 1.0]]}, "state_space_specs": {"type": "continuous", "bounds": [["Indoor Temperature", "-inf", "inf"], ["Outdoor Temperature", "-inf", "inf"], ["Setpoint", "-inf", "inf"], ["Deviation from Setpoint", "-inf", "inf"], ["Occupancy", "-inf", "inf"]]}, "learning_rate": 0.0003, "n_steps": 2048, "batch_size": 64, "n_epochs": 10, "gamma": 0.99, "gae_lambda": 0.95, "clip_range": 0.2, "clip_range_vf": null, "ent_coef": 0.0, "vf_coef": 0.5, "max_grad_norm": 0.5, "use_sde": false, "sde_sample_freq": -1, "target_kl": null, "seed": null}


def submit_annotation(timestep, title):
    url = BACKEND_BASE_URL + 'annotations/'
    requests.post(url, json={
        'timestep': timestep,
        'text': title
    })


if __name__ == '__main__':  # Only for testing and validation purposes!

    environment = gym.make(
        'gym_hvac:HVAC-v0',
        total_steps=150_000,
        continuous_action_space=True,
        seed=1,
        observations_space_combination=[1, 1, 1, 1, 0, 0, 0, 0, 1],
        reward_function='clipped'
    )

    observation = environment.reset()
    # change dynamic:
    action = 0

    rewards = []  # contains rewards of current episode

    reward = 0.0
    current_timestep = 1
    while True:
        # environment.render()
        response = requests.post(BACKEND_BASE_URL + BACKEND_GET_ACTION_ENDPOINT, json={
            'observation': observation.tolist(),
            'reward': reward
        })

        action = json.loads(response.content)['action'][0]
        observation, reward, done, info = environment.step(action)
        rewards.append(reward)

        current_timestep += 1

        if current_timestep == CHANGE_DYNAMIC_TIMESTEP:
            environment.HEATING_TEMPERATURE *= DYNAMIC_CHANGE_FACTOR
            submit_annotation(current_timestep, 'Changed Heating Device')
