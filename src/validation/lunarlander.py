import gym
import requests
import numpy as np
import json
import random

BACKEND_BASE_URL = 'http://127.0.0.1:8000/api/'
BACKEND_GET_ACTION_ENDPOINT = 'get_action/'

# state space: 8 continous
# action space: 4 discrete


def submit_external_data(title='Outside Temperatures', labels=('Deutschland', 'England', 'Spanien'), value_range=(0, 30)):
    url = BACKEND_BASE_URL + 'external_data/'
    requests.post(url, json={
        'unique_title': title,
        'labels': labels,
        'data': [[random.uniform(value_range[0], value_range[1]) for _ in range(len(labels))]]
    })


def submit_annotation(timestep=None, title='Good Weather'):
    url = BACKEND_BASE_URL + 'annotations/'
    requests.post(url, json={
        'timestep': timestep if timestep is not None else random.randint(10, 5000),
        'text': title
    })


if __name__ == '__main__':  # Only for testing and validation purposes!
    environment = gym.make('LunarLander-v2')
    observation = environment.reset()

    returns = []  # contains cumulative rewards of past episodes
    rewards = []  # contains rewards of current episode

    reward = 0.0
    i = 0
    while True:
        i += 1
        if len(returns) == 20:  # print average of last twenty returns
            print(np.mean(returns))
            returns = []

        # environment.render()
        response = requests.post(BACKEND_BASE_URL + BACKEND_GET_ACTION_ENDPOINT, json={
            'observation': observation.tolist(),
            'reward': reward
        })

        submit_external_data()
        submit_external_data('Inside Temperature')

        if i % 1000 == 999:
            submit_annotation(i)

        action = json.loads(response.content)['action']
        observation, reward, done, info = environment.step(action)
        rewards.append(reward)

        if done:  # we need a continuing environment, so episodic is transformed
            returns.append(np.sum(rewards))
            rewards = []
            observation = environment.reset()
