import json

import gym
import requests

BACKEND_BASE_URL = 'http://127.0.0.1:8000/api/'
BACKEND_GET_ACTION_ENDPOINT = 'get_action/'

# config (paste into configuration.json):
# {"algorithm": "dqn", "gamma": 0.995, "learning_rate": 0.00025, "buffer_size": 50000, "exploration_fraction": 0.05, "exploration_final_eps": 0.05, "exploration_initial_eps": 0.05, "train_freq": 1, "batch_size": 32, "double_q": true, "learning_starts": 1000, "target_network_update_freq": 500, "prioritized_replay": false, "prioritized_replay_alpha": 0.6, "prioritized_replay_beta0": 0.4, "prioritized_replay_eps": 1e-06, "param_noise": false, "seed": null, "state_space_specs": {"type": "continuous", "bounds": [["Indoor Temperature", -20.0, 50.0], ["Outdoor Temperature", -20.0, 50.0], ["Set Point", 10.0, 28.0], ["Deviation From Set Point", -50.0, 50.0], ["Occupancy Prediction", 0.0, 1.0]]}, "action_space_specs": {"type": "discrete", "number_of_discrete_values": 7}}


def submit_annotation(timestep, title):
    url = BACKEND_BASE_URL + 'annotations/'
    requests.post(url, json={
        'timestep': timestep,
        'text': title
    })


def gym_make_simplified_env(timesteps=200_000):
    return gym.make(
        'gym_hvac:HVAC-v0',
        total_steps=timesteps,
        use_weather_data=False,
        simplified_dynamics=True,
        use_probabilistic_occupancy=False,
        outdoor_temperature_bias=15,
        outdoor_temperature_amplitude=10,
    )


def execute_run(env):
    try:
        observation = env.reset()
        # change dynamic:
        action = 0

        rewards = []  # contains rewards of current episode

        reward = 0.0
        current_timestep = 1
        while True:
            # environment.render()
            response = requests.post(BACKEND_BASE_URL + BACKEND_GET_ACTION_ENDPOINT, json={
                'observation': observation.tolist(),
                'reward': reward
            })

            action = json.loads(response.content)['action']
            observation, reward, done, info = env.step(action)
            rewards.append(reward)

            current_timestep += 1
    except IndexError:
        pass


if __name__ == '__main__':
    pretraining_timesteps = 15_000
    training_timesteps = 15_000

    simplified_env = gym_make_simplified_env(pretraining_timesteps)
    normal_env = gym.make('gym_hvac:HVAC-v0', total_steps=training_timesteps)

    execute_run(simplified_env)
    submit_annotation(pretraining_timesteps, 'Pretraining Finished')
    execute_run(normal_env)
