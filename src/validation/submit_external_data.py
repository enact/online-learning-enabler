import requests
import random
import time


def submit_external_data(title='Outside Temperatures', labels=('Deutschland', 'England', 'Spanien'), value_range=(0, 30)):
    url = 'http://127.0.0.1:8000/api/external_data/'
    requests.post(url, json={
        'unique_title': title,
        'labels': labels,
        'data': [[random.uniform(value_range[0], value_range[1]) for _ in range(len(labels))]]
    })


if __name__ == '__main__':
    for i in range(1000):
        submit_external_data()
        time.sleep(0.1)
